@extends('layouts.app')

@section('title', ' | Settings | Opening')

@section('styles')
    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('css/jquery.timepicker.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')
    @php($opening = json_decode(auth()->user()->opening))
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Profile settings</h3>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger" align="center" id="error-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul style="list-style-type: none;">
                                    @foreach ($errors->all() as $error)
                                        <li >{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ url('settings/opening') }}" method="post" data-toggle="validator">
                            @csrf
                            <div class="form-group {{ $errors->has('sunday') ? 'has-error' : ''}}">
                                <label for="sunday" class="form-control-label">Sunday</label><br/>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="sunday" value="open" {{ old('sunday', @$opening->sunday->status) == 'open' ? 'checked' : '' }}>Open
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="sunday" value="close" {{ old('sunday', @$opening->sunday->status) == 'close' ? 'checked' : '' }}>Close
                                    </label>
                                </div>
                                <div class="form-check-inline" id="sunday-timepair" style="display: {{ old('sunday', @$opening->sunday->status) == 'open' ? 'inline-flex' : 'none' }}">
                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Morning</legend>
                                            <input type="text" name="sunday_morning_start" class="time start morning form-control" value="{{ old('sunday_morning_start', @$opening->sunday->morning->start) }}" {{ old('sunday', @$opening->sunday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="sunday_morning_end" class="time end morning form-control" value="{{ old('sunday_morning_end', @$opening->sunday->morning->end) }}" {{ old('sunday', @$opening->sunday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>

                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Afternoon</legend>
                                            <input type="text" name="sunday_afternoon_start" class="time start afternoon form-control" value="{{ old('sunday_afternoon_start', @$opening->sunday->afternoon->start) }}" {{ old('sunday', @$opening->sunday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="sunday_afternoon_end" class="time end afternoon form-control" value="{{ old('sunday_afternoon_end', @$opening->sunday->afternoon->end) }}" {{ old('sunday', @$opening->sunday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>
                                </div>
                                {!! $errors->first('sunday', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('sunday_morning_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('sunday_morning_end', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('sunday_afternoon_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('sunday_afternoon_end', '<p class="help-block">:message</p>') !!}
                            </div>
                            <hr/>

                            <div class="form-group {{ $errors->has('monday') ? 'has-error' : ''}}">
                                <label for="monday" class="form-control-label">Monday</label><br/>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="monday" value="open" {{ old('monday', @$opening->monday->status) == 'open' ? 'checked' : '' }}>Open
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="monday" value="close" {{ old('monday', @$opening->monday->status) == 'close' ? 'checked' : '' }}>Close
                                    </label>
                                </div>
                                <div class="form-check-inline" id="monday-timepair" style="display: {{ old('monday', @$opening->monday->status) == 'open' ? 'inline-flex' : 'none' }}">
                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Morning</legend>
                                            <input type="text" name="monday_morning_start" class="time start morning form-control" value="{{ old('monday_morning_start', @$opening->monday->morning->start) }}" {{ old('monday', @$opening->monday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="monday_morning_end" class="time end morning form-control" value="{{ old('monday_morning_end', @$opening->monday->morning->end) }}" {{ old('monday', @$opening->monday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>

                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Afternoon</legend>
                                            <input type="text" name="monday_afternoon_start" class="time start afternoon form-control" value="{{ old('monday_afternoon_start', @$opening->monday->afternoon->start) }}" {{ old('monday', @$opening->monday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="monday_afternoon_end" class="time end afternoon form-control" value="{{ old('monday_afternoon_end', @$opening->monday->afternoon->end) }}" {{ old('monday', @$opening->monday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>
                                </div>
                                {!! $errors->first('monday', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('monday_morning_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('monday_morning_end', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('monday_afternoon_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('monday_afternoon_end', '<p class="help-block">:message</p>') !!}
                            </div>
                            <hr/>

                            <div class="form-group {{ $errors->has('tuesday') ? 'has-error' : ''}}">
                                <label for="tuesday" class="form-control-label">Tuesday</label><br/>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="tuesday" value="open" {{ old('tuesday', @$opening->tuesday->status) == 'open' ? 'checked' : '' }}>Open
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="tuesday" value="close" {{ old('tuesday', @$opening->tuesday->status) == 'close' ? 'checked' : '' }}>Close
                                    </label>
                                </div>
                                <div class="form-check-inline" id="tuesday-timepair" style="display: {{ old('tuesday', @$opening->tuesday->status) == 'open' ? 'inline-flex' : 'none' }}">
                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Morning</legend>
                                            <input type="text" name="tuesday_morning_start" class="time start morning form-control" value="{{ old('tuesday_morning_start', @$opening->tuesday->morning->start) }}" {{ old('tuesday', @$opening->tuesday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="tuesday_morning_end" class="time end morning form-control" value="{{ old('tuesday_morning_end', @$opening->tuesday->morning->end) }}" {{ old('tuesday', @$opening->tuesday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>
                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Afternoon</legend>
                                            <input type="text" name="tuesday_afternoon_start" class="time start afternoon form-control" value="{{ old('tuesday_afternoon_start', @$opening->tuesday->afternoon->start) }}" {{ old('tuesday', @$opening->tuesday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="tuesday_afternoon_end" class="time end afternoon form-control" value="{{ old('tuesday_afternoon_end', @$opening->tuesday->afternoon->end) }}" {{ old('tuesday', @$opening->tuesday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>
                                </div>
                                {!! $errors->first('tuesday', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('tuesday_morning_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('tuesday_morning_end', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('tuesday_afternoon_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('tuesday_afternoon_end', '<p class="help-block">:message</p>') !!}
                            </div>
                            <hr/>

                            <div class="form-group {{ $errors->has('wednesday') ? 'has-error' : ''}}">
                                <label for="wednesday" class="form-control-label">Wednesday</label><br/>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="wednesday" value="open" {{ old('wednesday', @$opening->wednesday->status) == 'open' ? 'checked' : '' }}>Open
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="wednesday" value="close" {{ old('wednesday', @$opening->wednesday->status) == 'close' ? 'checked' : '' }}>Close
                                    </label>
                                </div>
                                <div class="form-check-inline" id="wednesday-timepair" style="display: {{ old('wednesday', @$opening->wednesday->status) == 'open' ? 'inline-flex' : 'none' }}">
                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Morning</legend>
                                            <input type="text" name="wednesday_morning_start" class="time start morning form-control" value="{{ old('wednesday_morning_start', @$opening->wednesday->morning->start) }}" {{ old('wednesday', @$opening->wednesday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="wednesday_morning_end" class="time end morning form-control" value="{{ old('wednesday_morning_end', @$opening->wednesday->morning->end) }}" {{ old('wednesday', @$opening->wednesday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>

                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Afternoon</legend>
                                            <input type="text" name="wednesday_afternoon_start" class="time start afternoon form-control" value="{{ old('wednesday_afternoon_start', @$opening->wednesday->afternoon->start) }}" {{ old('wednesday', @$opening->wednesday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="wednesday_afternoon_end" class="time end afternoon form-control" value="{{ old('wednesday_afternoon_end', @$opening->wednesday->afternoon->end) }}" {{ old('wednesday', @$opening->wednesday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>
                                </div>
                                {!! $errors->first('wednesday', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('wednesday_morning_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('wednesday_morning_end', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('wednesday_afternoon_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('wednesday_afternoon_end', '<p class="help-block">:message</p>') !!}
                            </div>
                            <hr/>

                            <div class="form-group {{ $errors->has('thursday') ? 'has-error' : ''}}">
                                <label for="thursday" class="form-control-label">Thursday</label><br/>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="thursday" value="open" {{ old('thursday', @$opening->thursday->status) == 'open' ? 'checked' : '' }}>Open
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="thursday" value="close" {{ old('thursday', @$opening->thursday->status) == 'close' ? 'checked' : '' }}>Close
                                    </label>
                                </div>
                                <div class="form-check-inline" id="thursday-timepair" style="display: {{ old('thursday', @$opening->thursday->status) == 'open' ? 'inline-flex' : 'none' }}">
                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Morning</legend>
                                            <input type="text" name="thursday_morning_start" class="time start morning form-control" value="{{ old('thursday_morning_start', @$opening->thursday->morning->start) }}" {{ old('thursday', @$opening->thursday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="thursday_morning_end" class="time end morning form-control" value="{{ old('thursday_morning_end', @$opening->thursday->morning->end) }}" {{ old('thursday', @$opening->thursday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>

                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Afternoon</legend>
                                            <input type="text" name="thursday_afternoon_start" class="time start afternoon form-control" value="{{ old('thursday_afternoon_start', @$opening->thursday->afternoon->start) }}" {{ old('thursday', @$opening->thursday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="thursday_afternoon_end" class="time end afternoon form-control" value="{{ old('thursday_afternoon_end', @$opening->thursday->afternoon->end) }}" {{ old('thursday', @$opening->thursday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>
                                </div>
                                {!! $errors->first('thursday', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('thursday_morning_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('thursday_morning_end', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('thursday_afternoon_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('thursday_afternoon_end', '<p class="help-block">:message</p>') !!}
                            </div>
                            <hr/>

                            <div class="form-group {{ $errors->has('friday') ? 'has-error' : ''}}">
                                <label for="friday" class="form-control-label">Friday</label><br/>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="friday" value="open" {{ old('friday', @$opening->friday->status) == 'open' ? 'checked' : '' }}>Open
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="friday" value="close" {{ old('friday', @$opening->friday->status) == 'close' ? 'checked' : '' }}>Close
                                    </label>
                                </div>

                                <div class="form-check-inline" id="friday-timepair" style="display: {{ old('friday', @$opening->friday->status) == 'open' ? 'inline-flex' : 'none' }}">
                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Morning</legend>
                                            <input type="text" name="friday_morning_start" class="time start morning form-control" value="{{ old('friday_morning_start', @$opening->friday->morning->start) }}" {{ old('friday', @$opening->friday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="friday_morning_end" class="time end morning form-control" value="{{ old('friday_morning_end', @$opening->friday->morning->end) }}" {{ old('friday', @$opening->friday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>

                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Afternoon</legend>
                                            <input type="text" name="friday_afternoon_start" class="time start afternoon form-control" value="{{ old('friday_afternoon_start', @$opening->friday->afternoon->start) }}" {{ old('friday', @$opening->friday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="friday_afternoon_end" class="time end afternoon form-control" value="{{ old('friday_afternoon_end', @$opening->friday->afternoon->end) }}" {{ old('friday', @$opening->friday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>
                                </div>
                                {!! $errors->first('friday', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('friday_morning_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('friday_morning_end', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('friday_afternoon_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('friday_afternoon_end', '<p class="help-block">:message</p>') !!}
                            </div>
                            <hr/>

                            <div class="form-group {{ $errors->has('saturday') ? 'has-error' : ''}}">
                                <label for="saturday" class="form-control-label">Saturday</label><br/>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="saturday" value="open" {{ old('saturday', @$opening->saturday->status) == 'open' ? 'checked' : '' }}>Open
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="saturday" value="close" {{ old('saturday', @$opening->saturday->status) == 'close' ? 'checked' : '' }}>Close
                                    </label>
                                </div>
                                <div class="form-check-inline" id="saturday-timepair" style="display: {{ old('saturday', @$opening->saturday->status) == 'open' ? 'inline-flex' : 'none' }}">
                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Morning</legend>
                                            <input type="text" name="saturday_morning_start" class="time start morning form-control" value="{{ old('saturday_morning_start', @$opening->saturday->morning->start) }}" {{ old('saturday', @$opening->saturday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="saturday_morning_end" class="time end morning form-control" value="{{ old('saturday_morning_end', @$opening->saturday->morning->end) }}" {{ old('saturday', @$opening->saturday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>

                                    <div class="timepair">
                                        <fieldset class="p-2">
                                            <legend>Afternoon</legend>
                                            <input type="text" name="saturday_afternoon_start" class="time start afternoon form-control" value="{{ old('saturday_afternoon_start', @$opening->saturday->afternoon->start) }}" {{ old('saturday', @$opening->saturday->status) == 'open' ? 'required' : '' }} /> to
                                            <input type="text" name="saturday_afternoon_end" class="time end afternoon form-control" value="{{ old('saturday_afternoon_end', @$opening->saturday->afternoon->end) }}" {{ old('saturday', @$opening->saturday->status) == 'open' ? 'required' : '' }} />
                                        </fieldset>
                                    </div>
                                </div>
                                {!! $errors->first('saturday', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('saturday_morning_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('saturday_morning_end', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('saturday_afternoon_start', '<p class="help-block">:message</p>') !!}
                                {!! $errors->first('saturday_afternoon_end', '<p class="help-block">:message</p>') !!}
                            </div>
                            <hr/>

                            <button type="submit" class="btn btn-primary btn-anim"> <i class="fa fa-rocket" aria-hidden="true"></i><span class="btn-text"> Submit</span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('vendor/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Jquery time-picker -->
    <script src="{{ asset('js/jquery.timepicker.min.js') }}"></script>
    <script src="{{ asset('js/datepair.js') }}"></script>
    <script src="{{ asset('js/jquery.datepair.js') }}"></script>


    <script>
        $(function() {
            // initialize input widgets first

            $('.timepair .time').timepicker({
                'showDuration': true,
                'timeFormat': 'g:i A',
            });
            $('.timepair .time.morning').timepicker({
                'showDuration': true,
                'timeFormat': 'g:i A',
                'minTime': '12:00am',
                'maxTime': '12:00pm',
            });
            $('.timepair .time.afternoon').timepicker({
                'showDuration': true,
                'timeFormat': 'g:i A',
                'minTime': '12:00pm',
                'maxTime': '12:00am',
            });
            $('.timepair').datepair({
                'defaultTimeDelta': 7200000,
                'anchor': 'start'
            });

            $('.form-check-input').change(function(){
                var name = $(this).attr("name");
                if($(this).val() == 'open'){
                    $('#'+name+'-timepair').show();
                    $('#'+name+'-timepair').find('input').attr('required', 'required');
                }
                else{
                    $('#'+name+'-timepair').hide();
                    $('#'+name+'-timepair').find('input').removeAttr('required');
                }
            })

        });
    </script>
@endsection
