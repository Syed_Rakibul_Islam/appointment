@extends('layouts.app')

@section('title', ' | Settings | Edit')

@section('styles')
    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('vendor/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap Wysihtml5 css -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css') }}" />
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Profile settings</h3>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger" align="center" id="error-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul style="list-style-type: none;">
                                    @foreach ($errors->all() as $error)
                                        <li >{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ url('settings') }}" method="post" data-toggle="validator" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group {{ $errors->has('org_name') ? 'has-error' : ''}}">
                                <label for="org_name" class="form-control-label">Organization Name *</label>
                                <input type="text" class="form-control" id="org_name" placeholder="Enter organization name" name="org_name" value="{{ old('org_name', auth()->user()->org_name) }}" maxlength="150" required>
                                {!! $errors->first('org_name', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('org_email') ? 'has-error' : ''}}">
                                <label for="org_email" class="form-control-label">Email</label>
                                <input type="email" class="form-control" id="org_email" placeholder="Enter organization email" name="email" value="{{ old('org_email', auth()->user()->org_email) }}" maxlength="150">
                                {!! $errors->first('org_email', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                                <label for="email" class="form-control-label">Contact No</label>
                                <input type="text" class="form-control" id="contact" placeholder="Enter organization contact no" name="contact" value="{{ old('contact', auth()->user()->contact) }}" maxlength="150">
                                {!! $errors->first('contact', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('organization_image') ? 'has-error' : ''}}">
                                <label for="organization_image" class="form-control-label mb-10">Image</label>
                                <input type="file" name="organization_image" class="form-control dropify" data-default-file="{{ !empty(auth()->user()->image) ? asset('images/upload/organization/' . auth()->user()->image) : asset('images/org-avatar.jpg') }}" data-min-width="32" data-min-height="32" data-max-width="1600" data-max-height="1600" />
                                {!! $errors->first('organization_image', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                <label for="address" class="form-control-label">Address</label>
                                <textarea class="form-control" id="address" placeholder="Enter address" name="address" maxlength="500"> {{ old('address', auth()->user()->address) }} </textarea>
                                {!! $errors->first('address', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
                                <label for="location" class="form-control-label">Location</label>
                                <input type="text" class="form-control" id="location" placeholder="Enter google map location url" name="location" value="{{ old('location', auth()->user()->location) }}" maxlength="255">
                                {!! $errors->first('location', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
                                <label for="website" class="form-control-label">Website url</label>
                                <input type="text" class="form-control" id="website" placeholder="Enter website url" name="website" value="{{ old('website', auth()->user()->website) }}" maxlength="255">
                                {!! $errors->first('website', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                <label for="description" class="control-label mb-10">Description</label>
                                <textarea name="description" placeholder="Enter description..." class="form-control textarea_editor" maxlength="100000" maxlength="1000"> {{ old('description', auth()->user()->description) }} </textarea>
                                {!! $errors->first('description', '<p class="help-block text-danger">:message</p>') !!}
                            </div>

                            <button type="submit" class="btn btn-primary btn-anim"> <i class="fa fa-rocket" aria-hidden="true"></i><span class="btn-text"> Submit</span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('vendor/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Dropify JavaScript -->
    <script src="{{ asset('vendor/dropify/dist/js/dropify.min.js') }}"></script>

    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('vendor/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";

            $('.textarea_editor').wysihtml5({
                toolbar: {
                    fa: true,
                    "link": true
                }
            });

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>
@endsection
