@extends('layouts.app')

@section('title', ' | Settings')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Profile settings</h3>
                    </div>
                    <div class="card-body">
                        @include('common.alert')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="profile-img">
                                    <img src="{{ !empty(auth()->user()->image) ? asset('images/upload/organization/' . auth()->user()->image) : asset('images/org-avatar.jpg') }}" class="img-fluid" alt=""/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ url('settings/edit') }}" class="btn btn-primary float-right" >Edit Profile</a>
                                <div>
                                    <h5>
                                        {{ auth()->user()->name }}
                                    </h5>
                                    <h6>
                                        {{ auth()->user()->email }}
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div>
                            <h3 class="text-center">Organization</h3>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{{ auth()->user()->org_name }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ auth()->user()->org_email }}</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>{{ auth()->user()->address }}</td>
                                </tr>
                                @if(!empty(auth()->user()->location))
                                    <tr>
                                        <td>Location</td>
                                        <td><a class="btn btn-link" href="{{ auth()->user()->location }}" target="_blank">{{ auth()->user()->location }}</a></td>
                                    </tr>
                                @endif
                                @if(!empty(auth()->user()->website))
                                <tr>
                                    <td>Website</td>
                                    <td><a class="btn btn-link" href="{{ auth()->user()->website }}" target="_blank">{{ auth()->user()->website }}</a></td>
                                </tr>
                                @endif
                                <tr>
                                    <td>Description</td>
                                    <td>{!! auth()->user()->description !!}</td>
                                </tr>
                                <tr>
                                    <td>Opening Hours</td>
                                    <td>
                                        <a href="{{ url('/settings/opening') }}" class="btn btn-primary float-right">{{ !empty(auth()->user()->opening) ? 'Edit' : 'Add' }}</a>
                                        <div class="clearfix"></div> <br/>
                                        @php($opening = json_decode(auth()->user()->opening))
                                        <ul class="list-group">
                                            @if(!empty($opening))
                                            @foreach($opening as $key => $day)
                                                <li class="list-group-item"><p class="float-right">{{ @$day->status == 'open' ? 'Morning: ' . @$day->morning->start . ' - to - ' . @$day->morning->end . ' | Afternoon: ' . @$day->afternoon->start . ' - to - ' . @$day->afternoon->end : ucfirst(@$day->status) }}</p>{{ ucfirst($key) }}</li>
                                            @endforeach
                                            @endif
                                        </ul>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
