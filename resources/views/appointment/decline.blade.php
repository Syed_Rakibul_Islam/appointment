@extends('appointment.layouts.frame')

@section('content')
    <div id="widget" style="position: relative; display: block;background-color: rgb(255, 255, 255); border-color: #000">
        <div class="header header-content" style="background-color: #313131;">
            <h1>{{ @$appointment->service->user->org_name }}</h1>
            <table class="nav">
                <tbody><tr>
                    <td class="nav-box done"></td>
                    <td class="nav-box nav-box-end"></td>
                </tr>
                </tbody></table>
        </div>
        <div class="content-page" style="display: block; height: 420px; overflow-y: auto;">
            <br/>
            <h4 style="font-weight: normal">This appointment has already been declined.</h4>

        </div>
    </div>
@endsection
