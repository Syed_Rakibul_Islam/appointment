<div class="date-header">
    @if($nextMonth)
    <span class="hand date-nav" onclick="changeMonth('{{ Carbon::parse($dateTime)->subMonth()->format('Y-m-d') }}')"><i class="fa fa-angle-left fa-lg"></i></span>
    @endif
    <span class="date-info" id="current-month" data-current-month="{{ Carbon::parse($dateTime)->format('m') }}" data-current-year="{{ Carbon::parse($dateTime)->format('Y') }}">{{ Carbon::parse($dateTime)->format('F Y') }}</span>
    <span class="hand date-nav next" onclick="changeMonth('{{ Carbon::parse($dateTime)->addMonth()->format('Y-m-d') }}')"><i class="fa fa-angle-right fa-lg"></i></span>
</div>

<table class="month" width="100%" cellspacing="2" cellpadding="0">
    <tbody><tr>

        <td class="day-title">Sun</td>

        <td class="day-title">Mon</td>

        <td class="day-title">Tue</td>

        <td class="day-title">Wed</td>

        <td class="day-title">Thu</td>

        <td class="day-title">Fri</td>

        <td class="day-title">Sat</td>

    </tr>
    <?php
    $j = 1;
    for($i = 1; $i <= Carbon::parse($dateTime)->daysInMonth; $i++){
        echo $j == 1 ? '<tr>' : '';
        $date = Carbon::parse(Carbon::parse($dateTime)->format('Y-m-')  . $i)->format('Y-m-d');
        $day = Carbon::parse(Carbon::parse($dateTime)->format('Y-m-')  . $i)->format('D');
        $closeDay = [];
        if(!empty($opening)){
            foreach($opening as $key => $value){
                if($value->status == 'close'){
                    $closeDay[] = ucfirst(substr($key, 0, 3));
                }
            }
        }
        if(in_array($day, $closeDay)){
            $aday = 'class="day dday" id="' . $date . '"';
        }
        elseif($i == Carbon::parse($dateTime)->format('d')){
            $aday = 'class="day sday" onclick="setDates(\'' . $date . '\')" id="' . $date . '"';
        }
        elseif($i >= Carbon::parse($today)->format('d') || $nextMonth){
            $aday = 'class="day" onclick="setDates(\'' . $date . '\')" id="' . $date . '"';
        }
        else{
            $aday = 'class="day dday" id="' . $date . '"';
        }

        if($j == 1){
            if($day == 'Sun'){
                echo '<td ' . $aday . '><span>' . $i . '</span></td>';
            }
            else{
                echo '<td class="day" id="x"><span>&nbsp;</span></td>';
                $i--;
            }
        }
        elseif($j == 2){
            if($day == 'Mon'){
                echo '<td ' . $aday . '><span>' . $i . '</span></td>';
            }
            else{
                echo '<td class="day" id="x"><span>&nbsp;</span></td>';
                $i--;
            }
        }
        elseif($j == 3){
            if($day == 'Tue'){
                echo '<td ' . $aday . '><span>' . $i . '</span></td>';
            }
            else{
                echo '<td class="day" id="x"><span>&nbsp;</span></td>';
                $i--;
            }
        }
        elseif($j == 4){
            if($day == 'Wed'){
                echo '<td ' . $aday . '><span>' . $i . '</span></td>';
            }
            else{
                echo '<td class="day" id="x"><span>&nbsp;</span></td>';
                $i--;
            }
        }
        elseif($j == 5){
            if($day == 'Thu'){
                echo '<td ' . $aday . '><span>' . $i . '</span></td>';
            }
            else{
                echo '<td class="day" id="x"><span>&nbsp;</span></td>';
                $i--;
            }
        }
        elseif($j == 6){
            if($day == 'Fri'){
                echo '<td ' . $aday . '><span>' . $i . '</span></td>';
            }
            else{
                echo '<td class="day" id="x"><span>&nbsp;</span></td>';
                $i--;
            }
        }
        elseif($j == 7){
            if($day == 'Sat'){
                echo '<td ' . $aday . '><span>' . $i . '</span></td>';
            }
            else{
                echo '<td class="day" id="x"><span>&nbsp;</span></td>';
                $i--;
            }
        }

        if($j == 7){
            echo '</tr>';
            $j = 1;
        }
        else{
            $j++;
        }
    }
    ?>
    </tbody></table>
