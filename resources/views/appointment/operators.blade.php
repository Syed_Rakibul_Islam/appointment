<h6 class="text-center tab-title">{{ $name }} with …</h6>
@foreach($operators as $operator)
    <div class="tab-sections">
        <div class="tab-section service-with" data-id="{{ $operator->id }}" data-name="{{ $operator->name }}">
            <div class="row">
                <div class="col-6">
                    <img src="{{ !empty($operator->image) ? url("images/upload/operator/" . $operator->image) : url('images/avatar.jpg') }}" width="50">
                    <span>{{ $operator->name }}</span>
                </div>
            </div>
        </div>
    </div>
@endforeach

