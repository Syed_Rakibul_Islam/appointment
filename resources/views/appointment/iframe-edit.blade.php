<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ config('app.name', 'Laravel') }} | Book</title>
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>


<link href="{{ asset('appointment_res/css/widget.css') }}" rel="stylesheet">

<link href="{{ asset('appointment_res/css/style.css') }}" rel="stylesheet">
<body style="max-width: 100%; overflow-x: hidden">
<div class="">
    <form id="appointment-form" action="{{ url('book/' . $appointment->uuid . '/update') }}" method="post">
        @csrf
        <input type="hidden" name="appointment_date" id="appointment_date" value="">
        <input type="hidden" name="appointment_time" id="appointment_time" value="">
        <section class="header">
            <h4>When are you available?</h4>
            <h5 class="text-center">Choose your service and we'll find you the right professional in our business.</h5>
            <!-- Circles which indicates the steps of the form: -->
            <div class="text-center mt-10">
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
            </div>
        </section>
        <!-- One "tab" for each step in the form: -->
        <div id="tab-area">
            <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            <div class="tab" id="tab-3">
                @include('appointment.schedules')
            </div>
            <div class="tab" id="tab-5">
                <h6 class="text-center tab-title" id="checkbox-title"></h6>
                <div class="tab-sections" >
                    <div class="row row-confirm" style="margin-top: 50px">
                        <div class="col-sm-6" style="text-align: right;">
                            <div class="edit-confirm-date" style="">
                                <h3 style="color: #A5ABA9; font-size: 14px; margin-bottom: 10px">Previous time and date</h3>
                                <h3 style="font-size: 14px; font-weight: bold" id="old-date">{{ Carbon::parse(@$appointment->schedule)->format('M d, Y h:i A') }}</h3>
                            </div>
                        </div>
                        <div class="col-sm-1 edit-confirm-date-arrow" style=""><i class="fa fa-long-arrow-right"></i></div>
                        <div class="col-sm-4 edit-confirm-date-block" style="">
                            <div class="edit-confirm-date-new" style="">
                                <h3 style="color: #A5ABA9; font-size: 14px; margin-bottom: 10px">New time and date</h3>
                                <h3 style="font-size: 14px; color: #7FAF1B; font-weight: bold" id="new-date">
                                    </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab" id="tab-6"></div>
            <hr/>
            <div class="text-center">
                <button class="btn btn-info" type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                <button class="btn btn-primary" type="button" id="nextBtn" style="display: none">Next</button>
                <button class="btn btn-success" type="submit" id="submitButton" style="display: none">Confirm Appointment</button>
            </div>
        </div>

    </form>
</div>

<script>
    var baseURL = "{{ url('/') }}";
    var serviceName = '{{ $name }}';
    var serviceDuration = '{{ $duration }}';
    var operatorName = '{{ @$appointment->operator->name }}';
    var tz = jstz.determine();
    var timezone = tz.name();
    var uuid = '{{ $user->uuid }}';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>
<script src="{{ asset('appointment_res/js/edit-main.js') }}"></script>
</body>
</html>
