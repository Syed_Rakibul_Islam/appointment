<h6 class="text-center tab-title">{{ $name }} on …</h6>

<div class="row" id="date-page">
    <div id="months" class="col-sm-6">
        @include('appointment.months')
    </div>
    <div id="dates" class="col-sm-6">
        @include('appointment.times')
    </div>
</div>


