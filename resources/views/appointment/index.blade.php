<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ config('app.name', 'Laravel') }} | Book</title>
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>

<link href="{{ asset('appointment_res/css/widget.css') }}" rel="stylesheet">

<link href="{{ asset('appointment_res/css/style.css') }}" rel="stylesheet">
<body>
<div class="container-fluid">
    <form id="appointment-form" action="{{ url('book') }}" method="post">
        @csrf
        <input type="hidden" name="uuid" id="uuid" value="{{ $user->uuid }}">
        <input type="hidden" name="service_id" id="service_id" value="">
        <input type="hidden" name="operator_id" id="operator_id" value="">
        <input type="hidden" name="appointment_date" id="appointment_date" value="">
        <input type="hidden" name="appointment_time" id="appointment_time" value="">
        <section class="header">
            <h3>How can we assist?</h3>
            <h4 class="text-center">Choose your service and we'll find you the right professional in our business.</h4>
            <!-- Circles which indicates the steps of the form: -->
            <div class="text-center mt-10">
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
            </div>
        </section>
        <!-- One "tab" for each step in the form: -->
        <div id="tab-area">
            <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            <div class="tab" id="tab-1">
                <h6 class="text-center tab-title">Appointment for ...</h6>
                <div class="tab-sections" >
                    @foreach($user->services as $service)
                        <div class="tab-section service-for" data-id="{{ $service->id }}" data-name="{{ $service->name }}" data-duration="{{ $service->duration }}" >
                            <div class="row">
                                <div class="col-6">
                                    <img src="{{ !empty($service->image) ? url("images/upload/service/" . $service->image) : url('images/service-avatar.png') }}" width="50">
                                    <span>{{ $service->name }}</span>
                                </div>
                                <div class="col-6">
                                    {{ $service->duration }} Minutes
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="tab" id="tab-2"></div>
            <div class="tab" id="tab-3"></div>
            <div class="tab" id="tab-4">
                <h6 class="text-center tab-title" id="contact-title"></h6>
                <div class="tab-sections" >
                    <div class="form-group radio_button">
                        <p><b>Make the appointment for:</b></p>
                        <label class="radio-inline" for="for">
                            <input type="radio" name="for" value="Me" checked> Me
                        </label>
                        <label class="radio-inline" for="for">
                            <input type="radio" name="for" value="Others"> Others
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="f_name">First name</label>
                        <input type="text" class="form-control" id="f_name" placeholder="Enter first name" name="f_name">
                    </div>
                    <div class="form-group">
                        <label for="l_name">Last Name</label>
                        <input type="text" class="form-control" id="l_name" placeholder="Enter last name" name="l_name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter E-mail ID" name="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Phone Number</label>
                        <input type="text" class="form-control" id="phone" placeholder="Enter phone no" name="phone">
                    </div>
                </div>
            </div>
            <div class="tab" id="tab-5">
                <h6 class="text-center tab-title" id="checkbox-title"></h6>
                <div class="tab-sections" >
                    <div class="form-group checkbox_button">
                        <p><b>Checkbox List:</b></p>
                        @foreach($user->options as $option)
                        <div class="checkbox">
                            <label><input type="checkbox" name="checkbox[]" value="{{ $option->id }}"> {{ $option->name }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="tab" id="tab-6"></div>
            <hr/>
            <div class="text-center">
                <button class="btn btn-info" type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                <button class="btn btn-primary" type="button" id="nextBtn" style="display: none">Next</button>
                <button class="btn btn-success" type="submit" id="submitButton" style="display: none">Confirm Appointment</button>
            </div>
        </div>

    </form>
</div>

<script>
    var baseURL = "{{ url('/') }}";
    var serviceName = '';
    var serviceDuration = '';
    var operatorName = '';
    var tz = jstz.determine();
    var timezone = tz.name();
    var uuid = '{{ $user->uuid }}';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>
<script src="{{ asset('appointment_res/js/main.js') }}"></script>

<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-XXXXX-Y', 'auto');
    ga('send', 'pageview');
</script>
<!-- End Google Analytics -->

</body>
</html>
