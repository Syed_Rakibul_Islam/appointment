<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ config('app.name', 'Laravel') }}</title>
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link href="{{ asset('appointment_res/css/style.css') }}" rel="stylesheet">

<style>
    .step {
        background-color: #fff;
        opacity: 1;
    }
    .gray-border {
        padding: 0;
        margin: 0;
        width: 100%;
        border-top:20px solid #aaaaaa;
    }
</style>
<body>
<div class="container-fluid">

    <section class="header">
        <h3>{{ @$appointment->service->user->org_name }}</h3>
        <!-- Circles which indicates the steps of the form: -->
        <div class="text-center mt-10">
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
        </div>
    </section>
    <div class="gray-border"></div>
    <!-- One "tab" for each step in the form: -->
    <div id="tab-area">
        @php
            $messageContent = @$message->top_section ? $message->top_section : config('messages.thank-you.top_section');
            $messageContent = str_replace('[[first_name]]', @$appointment->f_name, $messageContent);
            $messageContent = str_replace('[[last_name]]', @$appointment->l_name, $messageContent);
            $messageContent = str_replace('[[service]]', @$appointment->service->name, $messageContent);
            $messageContent = str_replace('[[operator]]', @$appointment->operator->name, $messageContent);
            $messageContent = str_replace('[[date]]', Carbon::parse(@$appointment->schedule)->format('M d, Y'), $messageContent);
            $messageContent = str_replace('[[time]]', Carbon::parse(@$appointment->schedule)->format('h:i A'), $messageContent);
        @endphp
        <br/><br/>
        {!! $messageContent !!}

        <hr/>
        <div class="text-center">
            @php($options = @json_decode(@$message->options))
            @if(@$options->button_name && @$options->button_url)
                <a href="{{ @$options->button_url }}" class="btn btn-info" {{ @$options->button_new_tab && $options->button_new_tab == 'yes' ? 'target="_blank"' : '' }}>{{ @$options->button_name }}</a>
            @endif
            @if(@$options->second_button_name && @$options->second_button_url)
                <a href="{{ @$options->second_button_url }}" class="btn btn-success" {{ @$options->second_button_new_tab && $options->second_button_new_tab == 'yes' ? 'target="_blank"' : '' }}>{{ @$options->second_button_name }}</a>
            @endif
            <button type="button" class="btn btn-primary" onclick="window.open('{{ url('/book/' . $appointment->uuid . '/print') }}', '', 'width=800,height=600')">{{ @$options->print_button_name && !empty($options->print_button_name) ? $options->print_button_name : 'PRINT CONFIRMATION' }}</button>
        </div>

    </div>
</div>

<script src="{{ asset('appointment_res/js/main.js') }}"></script>

</body>
</html>
