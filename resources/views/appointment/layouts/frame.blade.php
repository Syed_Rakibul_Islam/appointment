<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ @$appointment->service->user->org_name }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta property="og:url" content="{{ @$appointment->service->user->website }}">
    <meta property="og:title" content="{{ @$appointment->service->user->org_name }}">
    <meta property="og:description" content="{!! @$appointment->service->user->description !!}">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ @$appointment->service->user->org_name }}">
    <meta name="twitter:description" content="{!! @$appointment->service->user->description !!}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('appointment_res/css/scheduling.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('appointment_res/css/widget.css') }}">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

</head>
<body cz-shortcut-listen="true">
<div id="site" class="page">
    <div class="header"></div>
    <div class="left">
        <img id="logo" src="{{ !empty(@$appointment->service->user->image) ? asset('images/upload/organization/' . $appointment->service->user->image) : asset('images/org-avatar.jpg') }}" alt="{{ @$appointment->service->user->org_name }}">
        <div class="info hours">
            <label class="label">Opening Hours</label>
            @php($opening = json_decode(@$appointment->service->user->opening))
            <div class="content">
                @php($opening = json_decode(@$appointment->service->user->opening))
                <ul>
                    @if(!empty($opening))
                        @foreach($opening as $key => $day)
                            <li class="day clearfix closed">
                                <label>{{ ucfirst($key) }}</label>
                                <ul class="day-list">
                                    <li>{{ @$day->status == 'open' ? @$day->morning->start . ' - ' . @$day->afternoon->end : ucfirst(@$day->status) }}</li>
                                </ul>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <div class="info address">
            <label class="label">Address</label>
            <div class="content">
                {!! @$appointment->service->user->address !!}
                <p><a target="_blank" href="{{ @$appointment->service->user->location }}">Location <i class="fa fa-angle-right"></i></a></p>
            </div>
        </div>
        <div class="info contact">
            <label class="label">Contact</label>
            <div class="content">
                <ul>
                    <li>{{ @$appointment->service->user->contact }}</li>
                    <li><a href="{{ @$appointment->service->user->website }}" target="_blank">Website </a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="center">
        <div id="company-title">
            <h1 style="color: #1E5A95">{{ @$appointment->service->user->org_name }}</h1>
            <p><span>{!! @$appointment->service->user->description !!}</span></p><p></p>
        </div>
        <div class="iframe-container">
            @yield('content')
        </div>
    </div>
</div>
<script type="text/javascript">

    jQuery(document).ready(function(){

        if ($('#logo').length == 0) {
            $('.left').css('margin-top', $('#company-title').height() + 'px');
        }
    });

</script>

</body>
</html>
