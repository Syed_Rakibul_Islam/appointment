@php
    $day = strtolower(Carbon::parse($dateTime)->format('l'));
    $openingDay = '';
    $morningStart = 0;
    $morningDuration = 0;
    $afternoonStart = 0;
    $afternoonDuration = 0;
    if(!empty($opening)){
        foreach($opening as $key => $value){
            if($day == $key){
                $openingDay = $value;
                break;
            }
        }
    }
    if(@$openingDay->status == 'open'){
        $morningStart = ((int)Carbon::parse(@$openingDay->morning->start)->format('H') * 60) + (int)Carbon::parse(@$openingDay->morning->start)->format('i');
        $morningStartTime = Carbon::parse(@$openingDay->morning->start);
        $morningFinishTime = Carbon::parse(@$openingDay->morning->end);
        $morningDuration = $morningFinishTime->diffInMinutes($morningStartTime);

        if((int)Carbon::parse(@$openingDay->afternoon->start)->format('h') == 12){
            $afternoonStart = (int)Carbon::parse(@$openingDay->afternoon->start)->format('i');
        }
        else{
            $afternoonStart = ((int)Carbon::parse(@$openingDay->afternoon->start)->format('h') * 60) + (int)Carbon::parse(@$openingDay->afternoon->start)->format('i');
        }
        $afternoonStartTime = Carbon::parse(@$openingDay->afternoon->start);
        $afternoonFinishTime = Carbon::parse(@$openingDay->afternoon->end);
        $afternoonDuration = $afternoonFinishTime->diffInMinutes($afternoonStartTime);
    }
@endphp
<div class="date-header">
    @if($today < $dateTime)
    <span class="hand date-nav" onclick="setDates('{{ Carbon::parse($dateTime)->subDay()->format('Y-m-d') }}', {{ Carbon::parse($dateTime)->format('d') == 01 ? 1 : 0 }})"><i class="fa fa-angle-left fa-lg"></i></span>
    @endif
    <span class="date-info">{{ Carbon::parse($dateTime)->format('l, F j, Y') }}</span>
    <span class="hand date-nav" onclick="setDates('{{ Carbon::parse($dateTime)->addDay()->format('Y-m-d') }}', {{ Carbon::parse($dateTime)->daysInMonth == Carbon::parse($dateTime)->format('d') ? 1 : 0 }})"><i class="fa fa-angle-right fa-lg"></i></span>
</div>



<div id="dates-block">
    <table class="table-dates">
        <tbody><tr>
            <th>Morning</th>
            <th>Afternoon</th>
        </tr>
        <tr>

            <td>
                <ul class="list list-dates">

                    @for($i = 0; $i < $morningDuration; $i += $duration)
                        @php($time = (int)(($morningStart + $i) / 60) . ':' . sprintf("%02d", (int)(($morningStart + $i) % 60)))
                        <li>
                            <input type="button" class="button " value="{{ $time }} AM" onclick="selectDate('{{ Carbon::parse($dateTime)->format('Y-m-d') }}', '{{ $time }} AM')">
                        </li>
                    @endfor

                </ul>
            </td>


            <td>
                <ul class="list list-dates">

                    @for($i = 0; $i < $afternoonDuration; $i += $duration)
                        @php($hour = (int)(($afternoonStart + $i) / 60) == 0 ? 12 : (int)(($afternoonStart + $i) / 60))
                        @php($time = $hour . ':' . sprintf("%02d", (int)(($afternoonStart + $i) % 60)))
                        <li>
                            <input type="button" class="button " value="{{ $time }} PM" onclick="selectDate('{{ Carbon::parse($dateTime)->format('Y-m-d') }}', '{{ $time }} PM')">
                        </li>
                    @endfor

                </ul>
            </td>

        </tr>
        </tbody></table>
</div>
