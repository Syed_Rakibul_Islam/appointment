@extends('appointment.layouts.frame')

@section('content')
    <div id="widget" style="position: relative; display: block;">

        <div class="header header-content">
            <h1>{{ @$appointment->service->user->org_name }}</h1>
            <table class="nav">
                <tbody><tr>
                    <td class="nav-box done"></td>
                    <td class="nav-box nav-box-end"></td>
                </tr>
                </tbody></table>
        </div>
        <div class="content-page" style="display: block; height: 420px; overflow-y: auto;">
            <h2 class="change-title">You have an appointment for {{ @$appointment->service->name }} on {{ Carbon::parse(@$appointment->schedule)->format('M d, Y') }} at {{ Carbon::parse(@$appointment->schedule)->format('h:i A') }}.
            </h2>

            <div class="change-buttons">
                <a class="az-btn-pos" href="{{ url('book/' . $appointment->uuid . '/edit') }}"><i class="fa fa-refresh"></i> Reschedule Appointment</a>

                <a class="az-btn-neg" href="{{ url('book/' . $appointment->uuid . '/cancel') }}" onclick="event.preventDefault();document.getElementById('cancel-form').submit();"><i class="fa fa-trash-o"></i> Appointment Cancellation</a>
                <form id="cancel-form" method="POST" action="{{ url('book/' . $appointment->uuid . '/cancel') }}" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
@endsection
