@extends('appointment.layouts.frame')

@section('content')
    <div id="widget" style="position: relative; display: block;">

        <div class="header header-content">
            <h1>{{ @$appointment->service->user->org_name }}</h1>
            <table class="nav">
                <tbody><tr>
                    <td class="nav-box done"></td>
                    <td class="nav-box nav-box-end"></td>
                </tr>
                </tbody></table>
        </div>
        <div class="content-page" style="display: block; height: 420px; overflow-y: auto;">
            <br/>
            <h4>Thank you!</h4>
            <h4 style="font-weight: normal">Your appointment is now cancelled.</h4>

        </div>
    </div>
@endsection
