<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Online Scheduling</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="https://conversiontoolbox.net/console/scheduling/shared/css/shared.css">
        <link rel="stylesheet" type="text/css" href="https://conversiontoolbox.net/console/scheduling/shared/css/scheduling.css">
        <style type="text/css">

            body {
                background-color: #FFF;
                margin: 10px;
            }

            #scheduling-today {
                margin: 10px 0 0 0;
            }

            h1 {
                margin-bottom: 5px;
            }

            #abuttons {
                position: absolute;
                top: 15px;
                right: 15px;
            }

            h2 {
                font-size: 16px;
                font-weight: normal;
            }

            h2 strong {
                font-size: 16px;
            }

            fieldset {
                margin-top: 20px;
            }

            fieldset p {
                margin-bottom: 10px;
                line-height: 1.0;
                clear: left;
                overflow: auto;
            }

            fieldset label {
                display: inline-block;
                width: 150px;
                float: left;
                font-weight: normal;
                font-weight: bold;
                white-space: normal;
            }

            ul {
                display: inline-block;
            }


        </style>
        <style type="text/css" media="print">

            #abuttons {
                display: none;
            }

        </style>
    </head>
    <body cz-shortcut-listen="true">

        <div></div>
        <p style="margin-bottom: 20px;"><img src="{{ !empty(@$appointment->service->user->image) ? asset('images/upload/organization/' . $appointment->service->user->image) : asset('images/org-avatar.jpg') }}" alt="{{ @$appointment->service->user->org_name }}"></p>
        <h2>Appointment Confirmation</h2>
        <p>Appointment Reference: {{ $appointment->uuid }}</p>
        <p>Date: {{ Carbon::parse(@$appointment->schedule)->format('M d, Y') }}</p>
        <p>Time2: {{ Carbon::parse(@$appointment->schedule)->format('h:i A') }}</p>
        <p>Service: {{ @$appointment->service->name }}</p>
        <p>Stuff: {{ @$appointment->operator->name }}</p>
        <p>Address:</p>
        {{ @$appointment->service->user->address }}
    </body>
</html>
