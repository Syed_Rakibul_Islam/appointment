@extends('layouts.app')

@section('title', ' | Operators | ' . $operator->id)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3><a href="{{ url('operators') }}">Operators</a> <i class="fa fa-angle-double-right"></i> {{ $operator->id }}</h3>
                    </div>

                    <div class="card-body">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>ID</td>
                                <td>{{ $operator->id }}</td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>{{ $operator->name }}</td>
                            </tr>
                            <tr>
                                <td>Image</td>
                                <td><img src="{{ !empty($operator->image) ? asset('images/upload/operator/' . $operator->image) : asset('images/avatar.jpg') }}" alt="{{ $operator->name }}"/></td>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
