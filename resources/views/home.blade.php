@extends('layouts.app')

@section('title', ' | Home')

@section('styles')
    <!-- Event Calendar -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/timetable.css') }}"/>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Dashboard</h2>
                </div>

                <div class="card-body">
                    <label for="email">Appointment URL</label>
                    <input type="text" class="form-control" value="{{ url('book/' . auth()->user()->uuid) }}" readonly>
                    <p>Iframe source url</p>
                </div>
                <h2 class="text-center">Appointments</h2>
                <div class="card card-body">
                    <div class="tiva-timetable" data-source="php" data-view="list"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <!-- Event Calendar -->
    <script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('js/timetable.js') }}"></script>

@endsection
