
@component('mail::message', ['website' => $website, 'org_name' => $org_name])

    <table>
        <tr>
            <td>
                <table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="text-align: center">
                    <tbody>
                    <tr>
                        <td align="center">
                            <a target="_blank" href="https://viewstripo.email/"> <img src="https://img.icons8.com/dusk/64/000000/ok.png" alt="" style="display: block;" width="80"> </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <h1 style="text-align: center">APPOINTMENT HAS BEEN CANCELLED</h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 120px;">
                            <p style="font-size: 16px; color: #777777;">Dear <b>{{ $name }}</b>, <br/>
                                This email confirms your requested cancellation of your appointment with {{ $operator }} at {{ $time }} on {{ $date }}. I hope we can reschedule at your convenience.<br></p>
                            <p>See you soon,</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>

@endcomponent
