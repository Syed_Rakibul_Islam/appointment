@component('mail::message', ['website' => $website, 'org_name' => $org_name])

    <table>
        <tr>
            <td>
                <table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="text-align: center">
                    <tbody>
                    <tr>
                        <td align="center">
                            <a target="_blank" href="https://viewstripo.email/"> <img src="https://img.icons8.com/dusk/64/000000/ok.png" alt="" style="display: block;" width="80"> </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <h1 style="text-align: center">REMINDER</h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 120px;">
                            <p style="font-size: 16px; color: #777777;">Hi <b>{{ $name }}</b>, <br/>
                                I'm writing to remind you of your upcoming appointment.<br></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" cellspacing="0" cellpadding="0" style="border-top: 3px solid rgb(238, 238, 238);border-bottom: 3px solid rgb(238, 238, 238);">
                    <tbody>
                    <tr>
                        <td align="center" style="height: 80px;">
                            <h2 style="font-size: 24px;">{{ $service }} on {{ $date }} at {{ $time }}</h2>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                    <tbody>
                    <tr>
                        <td align="left" style="height: 80px;">
                            <p style="font-size: 16px; color: #777777;">If you would like to modify or cancel this appointment please use the button below.<br><br/>See you soon,</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                @component('mail::button', ['url' => $url, 'color' => 'green'])
                    Modify / Cancel Appointment
                @endcomponent
            </td>
        </tr>
    </table>

@endcomponent
