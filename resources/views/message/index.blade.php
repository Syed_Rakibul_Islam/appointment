@extends('layouts.app')

@section('title', ' | Messages')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Messages</h3>
                    </div>

                    <div class="card-body">
                        @include('common.alert')

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Page Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($names as $key => $value)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $value != 'thank-you' ? 'Email ' . ucfirst($value) : 'Thank You' }}</td>
                                    <td>
                                        <a href="{{ url('messages/' . $value) }}" class="btn btn-info pa-5 btn-sm" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="{{ url('messages/' . $value . '/edit') }}" class="btn btn-primary pa-5 btn-sm" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
