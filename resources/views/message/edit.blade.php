@extends('layouts.app')

@section('title', ' | Messages | ' . Route::input('name') . ' | Edit')

@section('styles')
    <!-- Bootstrap Wysihtml5 css -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css') }}" />
@endsection


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3><a href="{{ url('messages') }}">Messages</a> <i class="fa fa-angle-double-right"></i> <a href="{{ url('messages/' . Route::input('name')) }}">{{ Route::input('name') != 'thank-you' ? 'Email ' . ucfirst(Route::input('name')) : 'Thank You' }}</a> <i class="fa fa-angle-double-right"></i> Edit</h3>
                    </div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger" align="center" id="error-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul style="list-style-type: none;">
                                    @foreach ($errors->all() as $error)
                                        <li >{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ url('messages/' . Route::input('name')) . '/update' }}" method="post" data-toggle="validator">
                            @csrf
                            @php($topSection = isset($message->top_section) && !empty($message->top_section) ? $message->top_section : config('messages.thank-you.top_section'))
                            <span class="text-info">Short-codes: [[first_name]]  [[last_name]] [[service]] [[operator]] [[date]] [[time]]</span>
                            <hr/>
                            @if(Route::input('name') == 'thank-you')
                                <div class="form-group {{ $errors->has('top_section') ? 'has-error' : ''}}">
                                    <label for="top_section" class="form-control-label">Content *</label>
                                    <textarea name="top_section" placeholder="Enter content..." class="form-control textarea_editor" maxlength="100000" required> {!!  old('top_section', $topSection) !!} </textarea>
                                    {!! $errors->first('top_section', '<p class="help-block text-danger">:message</p>') !!}
                                </div>
                                @php($options = @json_decode(@$message->options))
                                @php($printButton = @$options->print_button_name && !empty($options->print_button_name) ? $options->print_button_name : 'PRINT CONFIRMATION' )
                                <div class="form-group {{ $errors->has('print_button_name') ? 'has-error' : ''}}">
                                    <label for="print_button_name" class="form-control-label">Print Button Name *</label>
                                    <input type="text" class="form-control" id="print_button_name" placeholder="Enter print button name" name="print_button_name" maxlength="150" value="{{ old('print_button_name', $printButton) }}" required>
                                    {!! $errors->first('print_button_name', '<p class="help-block text-danger">:message</p>') !!}
                                </div>
                                <hr/>
                                <div class="card bg-light">
                                    <div class="card-header text-center">Button</div>
                                    <div class="card-body">
                                        <div class="form-group {{ $errors->has('button_name') ? 'has-error' : ''}}">
                                            <label for="button_name" class="form-control-label">Button Name</label>
                                            <input type="text" class="form-control" id="button_name" placeholder="Enter button name" name="button_name" maxlength="150" value="{{ old('button_name', @$options->button_name) }}">
                                            {!! $errors->first('button_name', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('button_url') ? 'has-error' : ''}}">
                                            <label for="button_url" class="form-control-label">Button URL</label>
                                            <input type="url" class="form-control" id="button_url" placeholder="Enter button url" name="button_url" maxlength="500" value="{!! old('button_url', @$options->button_url)  !!}">
                                            {!! $errors->first('button_url', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('button_new_tab') ? 'has-error' : ''}}">
                                            <div class="checkbox">
                                                <label><input name="button_new_tab" type="checkbox" value="yes" {{ @$options->button_new_tab && $options->button_new_tab == 'yes' ? 'checked' : '' }}> Link open in a new tab.</label>
                                            </div>
                                            {!! $errors->first('button_new_tab', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="card bg-light">
                                    <div class="card-header text-center">Second Button</div>
                                    <div class="card-body">
                                        <div class="form-group {{ $errors->has('second_button_name') ? 'has-error' : ''}}">
                                            <label for="second_button_name" class="form-control-label">Second Button Name</label>
                                            <input type="text" class="form-control" id="second_button_name" placeholder="Enter second button name" name="second_button_name" maxlength="150" value="{{ old('second_button_name', @$options->second_button_name) }}">
                                            {!! $errors->first('second_button_name', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('second_button_url') ? 'has-error' : ''}}">
                                            <label for="second_button_url" class="form-control-label">Second Button URL</label>
                                            <input type="url" class="form-control" id="second_button_url" placeholder="Enter second button url" name="second_button_url" maxlength="500" value="{!! old('second_button_url', @$options->second_button_url)  !!}">
                                            {!! $errors->first('second_button_url', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('second_button_new_tab') ? 'has-error' : ''}}">
                                            <div class="checkbox">
                                                <label><input name="second_button_new_tab" type="checkbox" value="yes" {{ @$options->second_button_new_tab && $options->second_button_new_tab == 'yes' ? 'checked' : '' }}> Link open in a new tab.</label>
                                            </div>
                                            {!! $errors->first('second_button_new_tab', '<p class="help-block text-danger">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <button type="submit" class="btn btn-primary btn-anim"> <i class="fa fa-rocket" aria-hidden="true"></i><span class="btn-text"> Submit</span></button>
                            @else



                            @endif

                            {{--<button type="submit" class="btn btn-primary btn-anim"> <i class="fa fa-rocket" aria-hidden="true"></i><span class="btn-text"> Submit</span></button>--}}
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('vendor/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Dropify JavaScript -->
    <script src="{{ asset('vendor/dropify/dist/js/dropify.min.js') }}"></script>

    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('vendor/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";

            $('.textarea_editor').wysihtml5({
                toolbar: {
                    fa: true,
                    "link": true
                }
            });

        });
    </script>
@endsection
