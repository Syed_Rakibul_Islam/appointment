@extends('layouts.app')

@section('title', ' | Messages | ' . Route::input('name'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3><a href="{{ url('messages') }}">Messages</a> <i class="fa fa-angle-double-right"></i> {{ Route::input('name') != 'thank-you' ? 'Email ' . ucfirst(Route::input('name')) : 'Thank You' }}</h3>
                    </div>

                    <div class="card-body">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>Page Name</td>
                                <td>Thank You</td>
                            </tr>
                            @if(Route::input('name') == 'thank-you')
                                <tr>
                                    <td>Message</td>
                                    <td>
                                        {!! isset($message->top_section) && empty($message->top_section) ? $message->top_section : config('messages.thank-you.top_section') !!}
                                    </td>
                                </tr>
                                @php($extraButton = isset($message->options) && empty($message->options) ? @json_decode(@$message->options) : '' )
                                @if(!empty($extraButton))
                                    <tr>
                                        <td>Button Name</td>
                                        <td>{{ @$extraButton->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Button URL</td>
                                        <td>{{ @$extraButton->url }}</td>
                                    </tr>
                                @endif
                            @else
                                <tr>
                                    <td>Header</td>
                                    <td>
                                        {!! isset($message->header) && empty($message->header) ? $message->header : config('messages.' . Route::input('name') . '.header') !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Top Section</td>
                                    <td>
                                        {!! isset($message->top_section) && empty($message->top_section) ? $message->top_section : config('messages.' . Route::input('name') . '.top_section') !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Main Section</td>
                                    <td>
                                        {!! isset($message->main_section) && empty($message->main_section) ? $message->main_section : config('messages.' . Route::input('name') . '.main_section') !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bottom Section</td>
                                    <td>
                                        {!! isset($message->bottom_section) && empty($message->bottom_section) ? $message->bottom_section : config('messages.' . Route::input('name') . '.bottom_section') !!}
                                    </td>
                                </tr>
                            @endif

                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
