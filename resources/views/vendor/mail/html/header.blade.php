<tr style="background-color: #108561;" bgcolor="#108561">
    <td class="header">
        <a href="{{ $url }}" style="color: #fff;">
            {{ $slot }}
        </a>
    </td>
</tr>
