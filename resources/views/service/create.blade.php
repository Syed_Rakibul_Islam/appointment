@extends('layouts.app')

@section('title', ' | Services | Create')

@section('styles')

    <!-- select2 CSS -->
    <link href="{{ asset('vendor/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('vendor/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3><a href="{{ url('services') }}">Services</a> <i class="fa fa-angle-double-right"></i> Create</h3>
                    </div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger" align="center" id="error-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul style="list-style-type: none;">
                                    @foreach ($errors->all() as $error)
                                        <li >{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ url('services') }}" method="post" data-toggle="validator" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                <label for="name" class="form-control-label">Name *</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter service name" name="name" value="{{ old('name') }}" required>
                                {!! $errors->first('name', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('feature_image') ? 'has-error' : ''}}">
                                <label for="feature_image" class="form-control-label mb-10">Feature Image</label>
                                <input type="file" name="feature_image" class="form-control dropify" data-min-width="32" data-min-height="32" data-max-width="500" data-max-height="500" />
                                {!! $errors->first('feature_image', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
                                <label for="duration" class="form-control-label">Duration *</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="duration" placeholder="Enter duration" name="duration" min="5" max="500" value="{{ old('duration') }}" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Minutes</span>
                                    </div>
                                </div>
                                {!! $errors->first('duration', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('operators') ? 'has-error' : ''}}">
                                <label for="operators" class="form-control-label">Operators</label>
                                <select name="operators[]" class="form-control selectpicker" id="operators" data-style="form-control btn-default btn-outline" multiple>
                                    @foreach($operators as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('operators', '<p class="help-block">:message</p>') !!}
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-rocket" aria-hidden="true"></i><span class="btn-text"> Submit</span></button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('vendor/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Select2 JavaScript -->
    <script src="{{ asset('vendor/select2/dist/js/select2.full.min.js') }}"></script>

    <!-- Dropify JavaScript -->
    <script src="{{ asset('vendor/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";

            $('#operators').select2({
                placeholder: "Choose operators",
                allowClear: true,
                multiple:true
            });



            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>
@endsection
