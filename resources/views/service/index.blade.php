@extends('layouts.app')

@section('title', ' | Services')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary float-right" href="{{ url('services/create') }}" title="Create"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <h3>Services</h3>
                    </div>

                    <div class="card-body">
                        @include('common.alert')

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($services as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <a href="{{ url('services/' . $item->id) }}" class="btn btn-info pa-5 btn-sm" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="{{ url('services/' . $item->id . '/edit') }}" class="btn btn-primary pa-5 btn-sm" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                        <form method="POST" action="{{ url('services/' . $item->id) }}" class="d-inline">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger pa-5 btn-sm" title="Delete" onclick="return confirm('Confirm delete?')">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper float-right"> {!! $services->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
