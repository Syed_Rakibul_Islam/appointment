@extends('layouts.app')

@section('title', ' | Services | ' . $service->id)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3><a href="{{ url('services') }}">Services</a> <i class="fa fa-angle-double-right"></i> {{ $service->id }}</h3>
                    </div>

                    <div class="card-body">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>ID</td>
                                <td>{{ $service->id }}</td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>{{ $service->name }}</td>
                            </tr>
                            <tr>
                                <td>Duration</td>
                                <td>{{ $service->duration }} Minutes</td>
                            </tr>
                            @if(!empty($service->image))
                            <tr>
                                <td>Image</td>
                                <td><img src="{{ asset('images/upload/service/' . $service->image) }}" class="img-responsive"></td>
                            </tr>
                            @endif
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
