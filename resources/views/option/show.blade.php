@extends('layouts.app')

@section('title', ' | Options | ' . $option->id)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3><a href="{{ url('options') }}">Options</a> <i class="fa fa-angle-double-right"></i> {{ $option->id }}</h3>
                    </div>

                    <div class="card-body">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>ID</td>
                                <td>{{ $option->id }}</td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>{{ $option->name }}</td>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
