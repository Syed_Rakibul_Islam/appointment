@extends('layouts.app')

@section('title', ' | Options | ' . $option->id . ' | Edit')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3><a href="{{ url('options') }}">Options</a> <i class="fa fa-angle-double-right"></i> <a href="{{ url('options/' . $option->id) }}">{{ $option->id }}</a> <i class="fa fa-angle-double-right"></i> Edit</h3>
                    </div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger" align="center" id="error-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul style="list-style-type: none;">
                                    @foreach ($errors->all() as $error)
                                        <li >{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ url('options/' . $option->id) }}" method="post" data-toggle="validator">
                            @csrf
                            @method('PUT')
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                <label for="name" class="form-control-label">Name *</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter option name" name="name" value="{{ old('name', $option->name) }}" required>
                                {!! $errors->first('name', '<p class="help-block text-danger">:message</p>') !!}
                            </div>
                            <button type="submit" class="btn btn-primary btn-anim"> <i class="fa fa-rocket" aria-hidden="true"></i><span class="btn-text"> Submit</span></button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('vendor/bootstrap-validator/dist/validator.min.js') }}"></script>
@endsection
