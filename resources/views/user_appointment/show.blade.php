@extends('layouts.app')

@section('title', ' | Appointment | ' . $appointment->id)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                        @if($appointment->status != 'cancel')
                            <form method="POST" action="{{ url('appointments/' . $appointment->id) }}" class="d-inline float-right">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger pa-5 btn-sm" title="Delete" onclick="return confirm('Confirm cancel?')">
                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                </button>
                            </form>
                        @endif

                        <a href="{{ url('appointments') }}">Appointments </a> <i class="fa fa-angle-double-right"></i> {{ $appointment->id }}

                    </div>

                    <div class="card-body">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>ID</td>
                                <td>{{ $appointment->id }}</td>
                            </tr>
                            <tr>
                                <td>UUID</td>
                                <td>{{ $appointment->uuid }}</td>
                            </tr>
                            <tr>
                                <td>Appointee Name</td>
                                <td>{{ $appointment->f_name . ' ' . $appointment->l_name }}</td>
                            </tr>
                            <tr>
                                <td>Appointee Email</td>
                                <td>{{ $appointment->email }}</td>
                            </tr>
                            <tr>
                                <td>Appointee Phone</td>
                                <td>{{ $appointment->phone }}</td>
                            </tr>
                            <tr>
                                <td>Options</td>
                                <td>
                                    @if(!empty(@$appointment->options))
                                        <ul>
                                        @foreach(@$appointment->options as $option)
                                            <li> <a href="{{ url('options/' . $option->id) }}">{{ $option->name }}</a></li>
                                        @endforeach
                                        </ul>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Appointment for</td>
                                <td>{{ $appointment->for == 'Me' ? 'Self' : 'Others' }}</td>
                            </tr>
                            <tr>
                                <td>Service</td>
                                <td><a href="{{ url('services/' . @$appointment->service->id) }}">{{ @$appointment->service->name }}</a></td>
                            </tr>
                            <tr>
                                <td>Operator</td>
                                <td><a href="{{ url('operators/' . @$appointment->operator->id) }}">{{ @$appointment->operator->name }}</a></td>
                            </tr>
                            <tr>
                                <td>Start Time</td>
                                <td>{{ Carbon::parse($appointment->schedule)->format('M d, Y h:i A') }}</td>
                            </tr>
                            <tr>
                                <td>End Time</td>
                                <td>{{ Carbon::parse($appointment->schedule)->addMinutes(@$appointment->service->duration)->format('M d, Y h:i A') }}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    @if(empty($appointment->status))
                                        <p class="text-success"> Active </p>
                                    @else
                                        <p class="text-danger"> Canceled </p>
                                    @endif
                                </td>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
