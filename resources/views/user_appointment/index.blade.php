@extends('layouts.app')

@section('title', ' | Appointments')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Appointments</h3>
                    </div>

                    <div class="card-body">
                        @include('common.alert')

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($appointments as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ !empty($item->f_name) ? $item->f_name : $item->l_name }}</td>
                                    @if($item->status != 'cancel')
                                        <td class="text-success"> Active </td>
                                    @else
                                        <td class="text-danger"> Canceled </td>
                                    @endif
                                    <td>
                                        <a href="{{ url('appointments/' . $item->id) }}" class="btn btn-info pa-5 btn-sm" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        @if($item->status != 'cancel')
                                            <form method="POST" action="{{ url('appointments/' . $item->id) }}" class="d-inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger pa-5 btn-sm" title="Delete" onclick="return confirm('Confirm cancel?')">
                                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper float-right"> {!! $appointments->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
