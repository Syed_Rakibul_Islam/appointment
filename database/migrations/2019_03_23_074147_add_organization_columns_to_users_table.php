<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrganizationColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('org_name', 150)->nullable();
            $table->string('org_email', 150)->nullable();
            $table->string('image', 255)->nullable();
            $table->mediumText('address')->nullable();
            $table->string('location', 255)->nullable();
            $table->string('contact', 150)->nullable();
            $table->string('website', 255)->nullable();
            $table->longText('description')->nullable();
            $table->longText('opening')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['opening', 'description', 'website', 'contact', 'location', 'address', 'org_email', 'org_name',]);
        });
    }
}
