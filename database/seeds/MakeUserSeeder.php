<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class MakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $is_acc = User::find(1);
        if(empty($is_acc)) {
            User::create([
                'id' => 1,
                'uuid' => '78610740-49fe-11e9-80d5-8b8f5dc51983',
                'name' => 'bigsh0p.myshopify.com',
                'token' => '60d0c6b8ba89b2c8d3a5f954c12773e1',
                'install_date' => Carbon::now(),
                'charge_id' => '5407768687',
                'recuring_days' => 7,
                'org_name' => 'arcpoint labs',
                'org_email' => 'romeomyname@gmail.com',
                'address' => '2901 W Busch Blvd STE 206 Tampa 33618 FL  United States',
                'location' => 'https://www.google.com/maps?q=2901+W+Busch+Blvd+STE+206+Tampa+33618+US',
                'contact' => '8136185227',
                'website' => 'https://www.arcpointlabs.com/north-tampa',
                'description' => 'ARCpoint Labs is a full-service national third-party provider/administrator of accurate, reliable, and confidential diagnostic testing for individuals, companies, and legal and healthcare professionals. We’ve been in the drug and alcohol testing business for over 18 years, and our staff has over 60 years of combined industry experience.',
                'opening' => '{"sunday":{"status":"close"},"monday":{"status":"open","morning":{"start":"9:00 AM","end":"12:00 PM"},"afternoon":{"start":"1:00 PM","end":"4:00 PM"}},"tuesday":{"status":"open","morning":{"start":"9:00 AM","end":"12:00 PM"},"afternoon":{"start":"1:00 PM","end":"4:00 PM"}},"wednesday":{"status":"open","morning":{"start":"9:00 AM","end":"12:00 PM"},"afternoon":{"start":"1:00 PM","end":"4:00 PM"}},"thursday":{"status":"open","morning":{"start":"9:00 AM","end":"12:00 PM"},"afternoon":{"start":"1:00 PM","end":"4:00 PM"}},"friday":{"status":"open","morning":{"start":"9:00 AM","end":"12:00 PM"},"afternoon":{"start":"1:00 PM","end":"4:00 PM"}},"saturday":{"status":"close"}}'
            ]);
        }
        else {
            echo 'Already have a user account' . PHP_EOL . PHP_EOL;
        }
    }
}
