<?php

use App\Service;
use Illuminate\Database\Seeder;

class MakeServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $isItem = Service::find(1);
        if(empty($isItem)) {
            $service = Service::create([
                'id' => 1,
                'name' => 'Blood Work',
                'duration' => 20,
                'user_id' => 1
            ]);
            $service->operators()->attach([1]);
        }
        $isItem = Service::find(2);
        if(empty($isItem)) {
            $service = Service::create([
                'id' => 2,
                'name' => 'DNA Testing',
                'duration' => 30,
                'user_id' => 1
            ]);
            $service->operators()->attach([1]);
        }
        $isItem = Service::find(3);
        if(empty($isItem)) {
            $service = Service::create([
                'id' => 3,
                'name' => 'Finger Print',
                'duration' => 30,
                'user_id' => 1
            ]);
            $service->operators()->attach([1, 2]);
        }
        $isItem = Service::find(4);
        if(empty($isItem)) {
            $service = Service::create([
                'id' => 4,
                'name' => 'Others',
                'duration' => 15,
                'user_id' => 1
            ]);
            $service->operators()->attach([1, 2]);
        }
    }
}
