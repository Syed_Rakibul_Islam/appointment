<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MakeUserSeeder::class);
        $this->call(MakeOperatorSeeder::class);
        $this->call(MakeServiceSeeder::class);
    }
}
