<?php

use Illuminate\Database\Seeder;
Use App\Operator;

class MakeOperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $isItem = Operator::find(1);
        if(empty($isItem)) {
            Operator::create([
                'id' => 1,
                'name' => 'Lab Technician',
                'user_id' => 1
            ]);
        }
        else {
            echo 'Operator exist' . PHP_EOL . PHP_EOL;
        }
        $isItem = Operator::find(2);
        if(empty($isItem)) {
            Operator::create([
                'id' => 2,
                'name' => 'DOT Certified Drug Technician',
                'user_id' => 1
            ]);
        }
        else {
            echo 'Operator exist' . PHP_EOL . PHP_EOL;
        }
    }
}
