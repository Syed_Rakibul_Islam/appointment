<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Shopify app routes*/
Route::get('/', 'ShopifyAppController@index');
Route::get('/install', 'ShopifyAppController@install');
Route::get('/oauth', 'ShopifyAppController@oauth');
Route::get('/charge', 'ShopifyAppController@charge');
Route::get('/reinstall', 'ShopifyAppController@reinstall');
Route::get('/declined', 'ShopifyAppController@declined');

Route::get('/view', 'ShopifyAppController@view');

Route::view('/faq', 'faq');
Route::view('/instructions', 'instructions');
/* Shopify app routes */


Route::post('/book', 'AppointmentController@store');
Route::get('/book/{uuid}/thank-you', 'AppointmentController@thankYou');
Route::get('/book/{uuid}/print', 'AppointmentController@print');
Route::get('/book/{uuid}/change', 'AppointmentController@change');
Route::get('/book/{uuid}/edit', 'AppointmentController@edit');
Route::get('/book/{uuid}/iframe-edit', 'AppointmentController@iframeEdit');
Route::post('/book/{uuid}/update', 'AppointmentController@update');
Route::post('/book/{uuid}/cancel', 'AppointmentController@cancel');
Route::get('/book/{uuid}/canceled', 'AppointmentController@canceled');
Route::get('/book/{uuid}/declined', 'AppointmentController@declined');
Route::get('/book/{uuid}', 'AppointmentController@index');

// Ajax prefix
Route::group(['prefix' => 'ajax', 'middleware' => 'ajax'], function () {
    Route::get('/operators/{id}', 'AppointmentController@operators');


    Route::get('/schedules/change-month', 'AppointmentController@changeMonth');
    Route::get('/schedules/change-date', 'AppointmentController@changeDate');

    Route::get('/schedules/{id}', 'AppointmentController@schedules');
});

Route::middleware(['shopify-auth'])->group(function () {

    Route::get('{home}', 'HomeController@index')->where('home', 'home|dashboard');
    Route::resource('/operators', 'OperatorController');
    Route::resource('/services', 'ServiceController');
    Route::resource('/options', 'OptionController');
    Route::view('/settings', 'settings.index');
    Route::view('/settings/edit', 'settings.edit');
    Route::post('/settings', 'HomeController@updateSettings');
    Route::view('/settings/opening', 'settings.opening');
    Route::post('/settings/opening', 'HomeController@updateOpening');
    Route::get('/messages', 'MessageController@index');
    Route::get('/messages/{name}', 'MessageController@show');
    Route::get('/messages/{name}/edit', 'MessageController@edit');
    Route::post('/messages/{name}/update', 'MessageController@update');
    Route::get('/appointments', 'UserAppointment@index');
    Route::post('/appointments/{id}/cancel', 'UserAppointment@cancel')->where(['id' => '[0-9]+']);
    Route::get('/appointments/{id}', 'UserAppointment@show')->where(['id' => '[0-9]+']);
    Route::delete('/appointments/{id}', 'UserAppointment@ban')->where(['id' => '[0-9]+']);
    Route::get('/appointments/{uuid}', 'UserAppointment@view');
    Route::get('/timetable/timetables.php', 'HomeController@jsonData');
});

