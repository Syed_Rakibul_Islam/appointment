var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        // document.getElementById("nextBtn").innerHTML = "Next";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
}

function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    // if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
        //...the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false:
            valid = false;
        }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
}


$(function() {

    // $('#tab-1').find('.tab-section').click(function() {
    //     nextPrev(1);
    // });
    $('#tab-2').find('.tab-section').click(function() {
        nextPrev(1);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.lds-spinner')
        .hide()
        .ajaxStart(function() {
            $(this).show();
        })
        .ajaxStop(function() {
            $(this).hide();
        });
    $('.service-for').click(function(){
        var id = $(this).data('id');
        serviceName = $(this).data('name');
        serviceDuration = $(this).data('duration');
        if(id){
            $('#service_id').val('id');
            $.ajax({
                type: 'GET',
                url: baseURL + '/ajax/operators/' + id,
                data: {name: serviceName},
                success: function (result) {
                    $('#tab-2').html(result);
                    $('#service_id').val(id);
                    nextPrev(1);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    ajaxError(textStatus, errorThrown);
                }
            });
        }
    });

    $('#tab-area').on("click", '.service-with', function() {
        var id = $(this).data('id');
        operatorName = $(this).data('name');
        var service_id = $('#service_id').val();
        if(id){
            $('#operator_id').val('id');
            $.ajax({
                type: 'GET',
                url: baseURL + '/ajax/schedules/' + id,
                data: {name: serviceName, service_id: service_id, timezone: timezone, duration: serviceDuration, uuid: uuid},
                success: function (result) {
                    $('#tab-3').html(result);
                    $('#operator_id').val(id);
                    nextPrev(1);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    ajaxError(textStatus, errorThrown);
                }
            });
        }
    });

    $('#nextBtn').click(function(){
        if(validateForm()){
            nextPrev(1);
            if(currentTab == 4){
                $('#submitButton').show();
                $(this).hide();
            }
        }
    });
    $('#prevBtn').click(function(){
        $('#submitButton').hide();
        if(currentTab == 3){
            $('#nextBtn').show();
        }
    });


});

function changeMonth(d){
    $.ajax({
        type: 'GET',
        url: baseURL + '/ajax/schedules/change-month',
        data: {date: d, timezone: timezone, duration: serviceDuration, uuid: uuid},
        success: function (result) {
            $('#tab-3').find('#months').html(result);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            ajaxError(textStatus, errorThrown);
        }
    });
    $.ajax({
        type: 'GET',
        url: baseURL + '/ajax/schedules/change-date',
        data: {date: d, timezone: timezone, duration: serviceDuration, uuid: uuid},
        success: function (result) {
            $('#tab-3').find('#dates').html(result);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            ajaxError(textStatus, errorThrown);
        }
    });
}
function setDates(d, c){
    $('#tab-3').find('.sday').removeClass('sday');
    $('#tab-3').find('#' + d).addClass('sday');
    if(c){
        $.ajax({
            type: 'GET',
            url: baseURL + '/ajax/schedules/change-month',
            data: {date: d, timezone: timezone, duration: serviceDuration, uuid: uuid},
            success: function (result) {
                $('#tab-3').find('#months').html(result);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                ajaxError(textStatus, errorThrown);
            }
        });
    }
    $.ajax({
        type: 'GET',
        url: baseURL + '/ajax/schedules/change-date',
        data: {date: d, timezone: timezone, duration: serviceDuration, uuid: uuid},
        success: function (result) {
            $('#tab-3').find('#dates').html(result);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            ajaxError(textStatus, errorThrown);
        }
    });
}

function selectDate(d, t){
    console.log();
    $('#appointment_date').val(d);
    $('#appointment_time').val(t);
    var title = '<b>' +  serviceName + '</b> with <b>' + operatorName + ' </b> on  <b>' + d + ' ' + t + '</b>';
    $('#contact-title').html(title);
    $('#checkbox-title').html(title);
    $('#nextBtn').show();
    nextPrev(1);
}

function ajaxError(s, e){
    console.log("Status: " + s, "error");
    console.log("Error: " + e, "error");
    alert('Something is wrong. Please try again');
}
