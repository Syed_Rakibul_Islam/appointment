var WIDGET_MODE;
var DAY;

var CURRENT_DAY;
var CURRENT_MONTH;
var CURRENT_YEAR;
var INDEX = 0;
var LANG = 'en';
var DEFAULT_SERVICE = '0';
var VALIDATION;
var FPAGE_INDEX = 0;
var NONE_COUNT = 3;
var WIDGET;
var LOGIN_SERVICE;
var CUSTOM_ADD_FORM = false;

var ID_SERVICE_IN_URL = false ;
var ID_RESSOURCE_IN_URL = false

var SERVICE_RESOURCE_MODE;
var MULTIPLE_BOOKING ;
var MAX_COUNT;
var ITERATION_BOOKING;
var MAX_ITERATION;
var OVER_NIGHT;

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(search, pos) {
        return this.substr(!pos || pos < 0 ? 0 : +pos, search.length) === search;
    };
}

function WidgetEvent(type, listener) {
    this.type = type;
    this.listener = listener;
}

function Widget(options) {
    this.events = new Array();
}

Widget.prototype.addEventListener = function (type, listener) {
    this.events.push(new WidgetEvent(type, listener));
}

Widget.prototype.event = function (type, data) {

    for (var i = 0; i < this.events.length; i++) {
        if (this.events[i].type == type)
            this.events[i].listener(type, data);
    }
}

var WIDGET = new Widget();

function addAdditionalFields(objectId,objectType) {
    if ( ($('#widgetform input[name="id"]').val() === undefined ||  $('#widgetform input[name="id"]').val() === ""  ) && document.getElementById("widgetform") != null ) {
        var backoffice = $('#existing-client').length > 0;
        var companyID = $('#company').val();
        var account =  $('#account').val();
        var iframeQuery = document.location.search.substr(1) + '&wcompany=' + $('#company').val();

        if( iframeQuery.indexOf("lang") === -1 && $('#lang').length > 0 ){
            iframeQuery += "&lang="+$('#lang').val();
        }

        $('#fields > .page-content').load(WIDGET_ROOT + '/fields.jsp?'+ iframeQuery + '&' + objectType + "="+ objectId+((backoffice)?'&b=true':'')+((companyID)?'&wcompany='+companyID:''),function(){
            mandatory('widgetform');
            if(backoffice){
                initSearchClientInput('client');
                initSelectedClientData();
                $('#client').on('select2-selected', function(e) {
                    if (e.choice.status == 1) {
                        // Client lockÃ©
                        $('#client-locked-warning').show();
                    } else {
                        $('#client-locked-warning').hide();
                    }
                });
            }

        });
        $('#relationships > .page-content').load(WIDGET_ROOT + '/relationships.jsp?' + encodeURI(iframeQuery) + '&' + objectType + "="+ objectId+((backoffice)?'&b=true':'')+((companyID)?'&wcompany='+companyID:''),function(){
            mandatory('widgetform');
            if(backoffice){
                initSearchClientInput('client-relationships');
            }
        });

        $.ajax({
            method : 'GET',
            url : WIDGET_ROOT + '/fields-additional.jsp?'+ iframeQuery + ((backoffice)?'&b=true':'')+ ((account)?'&act='+account:''),
            async:false,
            data : 'withContainer=true&' + objectType + '=' + objectId,
            success : function(data) {
                $('.page.fields-page:not(#fields)').remove();
                $('.fa-fields-additional').remove();
                $('#bbuttons').before(data);
                var fieldPage = 0;
                $('.fields-page').each(function (i, element) {
                    if ($(element).find('.field').length !== 0) {
                        if(fieldPage > 0){
                            $('.nav-circles').append('<i class="fa fa-circle-thin fa-fields-additional"></i>');
                        }
                        fieldPage++;
                    }
                });
                hasAdditionalFields = fieldPage > 1;
                requiredToMandatory();
                mandatory('widgetform');
                initPageCSS();
                $('.field-date').pikaday({firstDay: 1, yearRange: 100, format: datePikaFormat.toUpperCase(),
                    i18n: {
                        months        : moment.localeData()._months,
                        weekdays      : moment.localeData()._weekdays,
                        weekdaysShort : moment.localeData()._weekdaysShort
                    }
                });

                setTimeout(function() { WIDGET.event('formDisplayed'); }, 200);
                if($('#client').length && !backoffice)
                    loadAdditionnalField($('#client').val());
            },
            error : function() {
                window.location.replace('/web/scheduling2/index-error.jsp?code=connection&retry=true&company='+companyID);
            }
        });

    }
}


function initSearchClientInput(idListClient){
    $('#'+idListClient).select2({
        minimumInputLength: 2,
        ajax: {
            url: "/console/scheduling/clients-search.jsp",
            dataType: 'json',
            data: function (term, page) {
                return { q: term };
            },
            results: function (data, page) {
                return { results: data };
            }
        },
        formatResult: clientFormatResult,
        formatSelection: clientFormatSelection,
        formatNoMatches: function(term){ return '<booking:message key="schedule.no.result"/>';},
        formatSearching: function(){ return '<booking:message key="schedule.search"/>';}
    });
}

function _serviceChange(service,max) {

    sendGoogleAnalyticsEvent('Selection',CINFOS,'Selected Service');

    if ($('input[type=radio][value=' + service + ']').length > 0)
        $('input[type=radio][value=' + service + ']').first().attr('checked', 'checked');

    WIDGET.event('serviceSelected', {
        serviceId: getService(),
        serviceName: $('#service-' + getService() + '-name').html()
    });

    if (WIDGET_MODE == 'service' || WIDGET_MODE == 'service-staff' || (WIDGET_MODE == 'staff-service' && $('#service').length > 0)) {
        $('#months').css('display', 'none');
        $('#date').css('display', 'none');
        $('#step2').css('display', 'none');
        var staffs = 0;

        //If more than 1 staff is available
        if ($('#stafflist').length > 0) {

            $('#stafflist li').css('display', 'none');
            $('#stafflist').find('li').each(function (i) {
                var id = $(this).attr('id');

                if ($(this).hasClass(getService())) {
                    $(this).css('display', 'block');
                    staffs ++;

                    if ($(this).find('.skill').length > 0) {
                        var l = new Array(3, 6, 9);
                        for (var j = 0; j < l.length; j++) {
                            if ($(this).find('.skill').hasClass(getService() + '-' + l[j]))
                                $('#level-' + id).attr('src', SERVER_ROOT + '/shared/img/skill-level-' + l[j] + '.png');
                        }
                    }

                    if ($(this).find('.price').length > 0) {
                        if (($(this).find('.price').eq(0).data(getService())))
                            $(this).find('.price').eq(0).html(($(this).find('.price').eq(0).data(getService())));
                    }
                }
            });
            $('.staff-random').css('display', staffs > 1 ? 'block' : 'none');
            if ($('input[type=radio][value=' + service + ']').length > 0)
                nextPage();

        } else if ($('.page').eq(getCurrentIndexPage()).attr('id') != 'companies' && $('.page').eq(getCurrentIndexPage()).attr('id') != 'login' ) {
            //If more than 1 service is available but the staff selection is not allowed or useless
            if ($('#servicelist').length > 0) {
                nextPage();
            } else if ( $('.options-page').length === 0 ) {
                _month();
            }
        }
    } else {
        nextPage();
    }

    var maxIteration = $('input[type=radio][name=service]:checked').data('dailymax');
    showOption(max, maxIteration);
    addAdditionalFields(service,'service');
}

function _resourceChange(resource, max) {

    if ($('input[type=radio][value=' + resource + ']').length > 0)
        $('input[type=radio][value=' + resource + ']').first().attr('checked', 'checked');

    WIDGET.event('resourceSelected', {
        resourceId: getResource(),
        resourceName: $('#resource-' + getService() + '-name').html()
    });

    if ($('#resources').length !== 0) {
        nextPage();
    }
    showOption(max);

    addAdditionalFields(resource,'resource');
}

function getStaff() {
    if (WIDGET_MODE == 'service' || WIDGET_MODE == 'resource')
    /*if ($('#staff').length > 0)
     return $('#staff').val();
     else*/
        return '0';
    else {

        if ($('#staff').length > 0)
            return $('#staff').val();
        else
            return $('input[type=radio][name=staff]:checked').attr('value');
    }
}

function getService() {
    if ($('#service').length > 0)
        return $('#service').val();
    else
        return $('input[type=radio][name=service]:checked').attr('value');
}

function getResource() {
    if ($('#resource').length > 0)
        return $('#resource').val();
    else
        return $('input[type=radio][name=resource]:checked').attr('value');
}

function getParameters() {

    var url = 'company=' + $('#company').val();

    //alert(getStaff());

    //if (getStaff())
    url += '&staff=' + getStaff();

    if ($('#id').length > 0)
        url += '&id=' + $('#id').val();

    //alert(getService());

    if (getService() && getService().length > 0) {
        url += '&service=' + getService();
    }

    if (getResource() && getResource().length > 0) {
        url += '&resource=' + getResource();
    }

    var itemCount = $('#rcount');
    if ( itemCount.length !== 0 ) {
        url += '&rcount=' + itemCount.val();
    }
    var itemIteration = $('#riterations');
    if ( itemIteration.length !== 0) {
        url += '&riterations=' + itemIteration.val();
    }

    var substituteDate = new Date();

    if($('#day').val() === null || $('#day').val() === "" || $('#month').val() === null || $('#month').val() === "" || $('#year').val() === null || $('#year').val() === "") {
        url += '&day=' + substituteDate.getDate() + '&month=' + substituteDate.getMonth() + '&year=' + substituteDate.getFullYear();
    }
    else {
        url += '&day=' + $('#day').val() + '&month=' + $('#month').val() + '&year=' + $('#year').val();
    }

    url += '&d=' + substituteDate.getTime() + '&lang=' + LANG + '&locale=' + $('#locale').val();

    if ($('#timezone').val())
        url += '&timezone=' + $('#timezone').val();

    if ($('#staffGroup').length > 0)
        url += '&staffGroup=' + $('#staffGroup').val();

    url += '&schedulingMode=' + WIDGET_MODE;

    //alert(url);
    return url;
}

function _changeMonth(month, year, day) {
    $('#month').val(month);
    $('#year').val(year);

    _month(day);
}

function _clickStaff(id) {

    $('input:radio[name="staff"]').filter('[value="' + id + '"]').attr('checked', true);
    _changeStaff();
}

function _changeStaff() {

    sendGoogleAnalyticsEvent('Selection',CINFOS,'Selected Staff');

    WIDGET.event('staffSelected', {staffId: getStaff(), staffName: $('#staff-' + getService() + '-name').html()});

    if (WIDGET_MODE == 'service' || WIDGET_MODE == 'service-staff' && $('#service').length > 0 || (WIDGET_MODE == 'staff-service' && $('#service').length > 0 && $('#staff').length === 0)) {

        highlightStaff(getStaff());
        nextPage();
        if ($('.page').eq(getCurrentIndexPage()).attr('id') == 'date'){
            _month();
        }
    } else {
        highlightStaff(getStaff());
        $('#a').css('display', 'block');

        $('#service').find('option').css('display', 'none');
        $('#service').find('option').removeAttr("selected");
        $('#service').find('option').attr('disabled', 'disabled');

        var selected = $('#service').attr('ds');
        var j = 0;
        var firstGroup = null;

        $('#servicelist').find('li').css('display', 'none');
        $('.group').css('display', 'none');

        $('#servicelist').find('li').each(function (i) {

            if ($(this).hasClass(getStaff())) {

                var serviceId = $(this).attr('id');

                $(this).css('display', 'block');

                if ($(this).find('.price').data(getStaff())) {

                    $(this).find('.price').html($(this).find('.price').data(getStaff()));
                }

                $('.group').each(function (i) {

                    var data = '' + $(this).data('services');

                    if (data.search(serviceId) != -1) {
                        $(this).css('display', 'block');

                        if (!firstGroup)
                            firstGroup = $(this).attr('id');

                    }
                });
            }
        });

        $('#' + firstGroup).trigger('click');

        if ($('#service').length > 0)
            _month();
        else if ($('#servicelist').length > 0 && $('#stafflist').length > 0) {
            nextPage();
            if ($('.page').eq(getCurrentIndexPage()).attr('id') == 'date') {
                _month();
            }
        }

    }
}

function _month(day) {

    $('#ajaxloader').css('display', 'block');


    var url = WIDGET_ROOT + '/month.jsp?' + getParameters();

    if (eval($('#current-month').data('current-year')) > eval($('#year').val()) || eval($('#current-month').data('current-month')) > eval($('#month').val()))
        url += '&past=true';

    $('#months').empty();

    $('#months').load(url, function () {
        $('#ajaxloader').css('display', 'none');

        var displayDate = false;
        var defaultDay = eval($('#current-month').data('first-day'));

        if ($('#months')[0].hasAttribute('defaultDay') && eval($('#current-month').data('current-month')) == eval($('#month').val())) {
            defaultDay = eval($('#day').val());
        }

        if (defaultDay != 0) {
            day = defaultDay + '.' + $('#current-month').data('current-month') + '.' + $('#current-month').data('current-year');
            displayDate = true;
        }

        if (day) {

            DAY = day;
            $('td[id="' + DAY + '"]').addClass('sday');

            if (displayDate)
                setDates(document.getElementById(day), eval(day.split('.')[0]), eval(day.split('.')[1]), eval(day.split('.')[2]));
        }
    });



    $('#months').css('display', 'block');

    if (!day)
        $('#dates').css('display', 'none');

    $('#date').css('display', 'none');

    $('#step2').css('display', 'block');

    $('#staffs').css('display', 'none');
    $('#date').css('display', 'block');


    if (WIDGET_MODE == 'none' && $('#companylist').length > 0) {
        $('#months').css('display', 'none');
        $('#date').css('display', 'none');
    }
}

function setDates(elm, day, month, year) {


    sendGoogleAnalyticsEvent('Selection',CINFOS,'Selected Day');

    WIDGET.event('dateSelected', {date: year + '-' + month + '-' + day});

    //$('#date').css('display', 'none');

    //alert($('.fields-page').length);

    //$('.fields-page').eq(0).css('display', 'block');

    $('td[id="' + DAY + '"]').removeClass('sday');

    if (elm) {
        DAY = elm.id;
        $('td[id="' + DAY + '"]').addClass('sday');
    }

    $('#day').val(day);
    $('#month').val(month);
    $('#year').val(year);

    if (eval($('#current-month').data('current-month')) != month)
        _changeMonth(month, year, '' + day + '.' + month + '.' + year);

    _dates();
}

var c=0;
function _dates() {

    $('#dates').css('display', 'block');
    $('#dates').html('');

    if ($('#ajaxloader').css('display') == 'none')
        $('#dates').html('<img src="' + WIDGET_ROOT + '/ajax-loader.gif" class="date-loader"/>');

    var url = WIDGET_ROOT + '/dates.jsp?' + getParameters();
    /*if ($('#hour').length > 0 && $('#hour').val()!="0") {
        url += '&monthPreselected=' + USER_PRESELECTED_MONTH + '&dayPreselected=' + USER_PRESELECTED_DAY + '&hour=' + $('#hour').val() + '&minutes=' + $('#minutes').val();
    }*/
    if ($('#hour').length > 0) {
        url += '&preselected=true&monthPreselected=' + USER_PRESELECTED_MONTH + '&dayPreselected=' + USER_PRESELECTED_DAY + '&hour=' + $('#hour').val() + '&minutes=' + $('#minutes').val();
    }

    url+= '&nonce=' + $('input[name="nonce"]').val();

    $('#dates').load(url, function (data) {

        if ($(document).width() > 600) {
            if ($('.timezone').length == 1 && WIDGET_MODE == 'none')
                $('#dates-block').css('height', '' + ($('.page').height() - 175));
            else
                $('#dates-block').css('height', '' + ($('.page').height() - 90));
        } else
            $('#dates-block').css('height', 'auto');

        if (WIDGET_MODE == 'none') {

            $('.choicedate').each(function (i) {

                var current = $('#year').val() + "-" + $('#month').val() + "-" + $('#day').val();

                if ($(this).data('date') == $('#year').val() + "-" + $('#month').val() + "-" + $('#day').val()) {
                    $('#' + $(this).data('hour') + "-" + $(this).data('minute')).css('display', 'none');
                }
            });
        }

        WIDGET.event('timesDisplayed', {date: year + '-' + month + '-' + day});


        var backOffice = ($('#existing-client').length > 0);

        $('#b-previous').on('click', function() {
            $('#b-next2').css('display', 'none');
            $('.good-date').css('color','black');
            $('.next-date').css('color','black');

        });


        if($('.good-date').length>0) {
            c=1;
            $('.good-date').each(function (i) {
                $('.good-date').css("background-color", "transparent");
                $('.next-date').css("background-color", "transparent");

                $('#b-next2').css('display', 'none');
                selectDate($(this).data('time'), $(this).data('full'));
            });
        }else if($('.next-date').length>0){
            c=1;
            $('.next-date').on('click', function() {
                $('.next-date').css("background-color","transparent");
                $('#b-next2').css('display', 'none');
                selectDate($('.next-date').data('time'), $('.next-date').data('full'));
            });
            $('.next-date').each(function(i) {
                $('#b-next').css('display', 'none');
                $('#b-next2').css('display', 'inline-block');
                $('#b-next2').on('click', function() {
                    $('.next-date').css("background-color","transparent");
                    $('#b-next2').css('display', 'none');
                    selectDate($('.next-date').data('time'), $('.next-date').data('full'));
                });
            });
        }else if($('.good-date').length==0&&$('.next-date').length==0&&c==0){
            c=1;


            if($(".choose-day-morning").length>0){
                $(".choose-day-morning:first").css("background-color","#eef8fb");
                $('#b-next').css('display', 'none');
                $('#b-next2').css('display', 'inline-block');

                $('#b-next2').on('click', function() {
                    $(".choose-day-morning:first").css("background-color","transparent");
                    $('#b-next2').css('display', 'none');

                    selectDate($(".choose-day-morning:first").data('time'), $(".choose-day-morning:first").data('full'));
                });

            }

            if($(".choose-day-afternoon").length>0){
                $(".choose-day-morning:first").css("background-color","transparent");
                $(".choose-day-afternoon:last").css("background-color","#eef8fb");
                $('#b-next').css('display', 'none');
                $('#b-next2').css('display', 'inline-block');

                $('#b-next2').on('click', function() {
                    $(".choose-day-afternoon:last").css("background-color","transparent");
                    $('#b-next2').css('display', 'none');

                    selectDate($(".choose-day-afternoon:last").data('time'), $(".choose-day-afternoon:last").data('full'));
                });

            }

        }
        $('.day').on('click', function() {

            $('#b-next2').css('display', 'none');

        });

        $('.choose-day-morning').on('click', function() {
            $('.next-date').css('color','black');
            $('.next-date').css("background-color","transparent");
            $('#b-next2').css('display', 'none');

        });

        $('.choose-day-afternoon').on('click', function() {
            $('.next-date').css('color','black');
            $('.next-date').css("background-color","transparent");
            $('#b-next2').css('display', 'none');

        });

    });

    $('#months').css('display', 'block');
    $('#step2').css('display', 'block');
}

function showInfos(time) {

    if (time == '') {
        $('#bnext').css('display', 'none');
    }

    $('#start').val(time);

    $('#step1').css('display', 'none');
    $('#step2').css('display', 'none');
    $('#step3').css('display', 'block');

    $('#bpdates').css('display', 'block');

    if ($('.fields-page').length < 2) {
        if (VALIDATION != 'none')
            $('#bvalidation').css('display', 'block');
        else
            $('#bsubmit').css('display', 'block');
    } else {
        $('#bnextform').css('display', 'block');
        //$('#bpdates').css('display', 'none');
    }


    $('#help').css('display', 'none');
    //$('#bbuttons').css('display', 'block');

    if ($('#id').length > 0 && false)
        $('#widgetform').submit();
    else {
        nextPage();
    }
}

function selectDate(time, display) {


    sendGoogleAnalyticsEvent('Selection',CINFOS,'Selected Hour');

    WIDGET.event('timeSelected', {time: time});

    if (WIDGET_MODE == 'none' && ($('#id').val() === undefined || $('#id').val() === 0)) {
        var index = $('.qdate').length;


        if (index > 0) {
            //alert(index);

            var date = $(document.createElement('button'));
            var close = $(document.createElement('close'));

            var elem = time.split(':');

            var d = new Date(eval($('#year').val()), eval($('#month').val()), eval($('#day').val()), eval(elem[0]), eval(elem[1]), 0, 0);

            date.addClass('choicedate');
            date.html(display);

            var start = $(document.createElement('input'));
            start.attr('type', 'hidden');
            date.append(start);
            start.attr('name', 'start_' + INDEX);
            start.attr('value', time);

            var year = $(document.createElement('input'));
            year.attr('type', 'hidden');
            date.append(year);
            year.attr('name', 'year_' + INDEX);
            year.attr('value', $('#year').val());

            var month = $(document.createElement('input'));
            month.attr('type', 'hidden');
            date.append(month);
            month.attr('name', 'month_' + INDEX);
            month.attr('value', $('#month').val());

            var day = $(document.createElement('input'));
            day.attr('type', 'hidden');
            date.append(day);
            day.attr('name', 'day_' + INDEX);
            day.attr('value', $('#day').val());

            INDEX++;

            close.html('&times');
            close.addClass('qclose');

            $('#' + elem[0] + "-" + elem[1]).css('display', 'none');
            date.data('hour', elem[0]);
            date.data('minute', elem[1]);
            date.data('date', $('#year').val() + "-" + $('#month').val() + "-" + $('#day').val());

            //alert(date.html());

            date.append(close);

            date.on('click', function () {

                $('#' + $(this).data('hour') + "-" + $(this).data('minute')).css('display', 'inherit');
                $(this).remove();

                INDEX--;

                //var empty = $(document.createElement('button'));
                var empty = $('<button type="button">');
                empty.html('&nbsp;');
                empty.addClass('button');
                empty.addClass('qdate');
                //empty.attr('type', 'button');

                $('#qdates').append(empty);

                if ($('.choicedate').length != NONE_COUNT){
                    $('#b-next').css('display', 'none');
                }
            });

            date.addClass('button');

            //$('#qdates').prepend(date);
            $('.qdate').first().replaceWith(date);
            //$('.qdate').prepend(date);

            if (INDEX == NONE_COUNT) {
                $('#b-next').css('display', 'inline-block');
                //$('#bbuttons').css('display', 'inline-block');

                if (getService())
                    $('#b-previous').css('display', 'inline-block');
                else
                    $('#b-previous').css('display', 'none');
            }
            else{
                $('#b-next').css('display', 'none');
            }

            if (INDEX == NONE_COUNT && NONE_COUNT == 1)
                showInfos(time);
            //$('.choicedate').length
        }
    }
    else {

        showInfos(time);
    }
}

function changeDayPeriod(s) {

    var ok = 'morning';
    var ko = 'afternoon';

    if (s == 'afternoon') {
        ok = 'afternoon';
        ko = 'morning';
    }

    $('#b' + ok).css('font-weight', 'bold');
    $('#b' + ko).css('font-weight', 'normal');

    $('#' + ok).css('display', 'block');
    $('#' + ko).css('display', 'none');
}

function showDates() {

    $('#step1').css('display', 'block');
    $('#step2').css('display', 'block');
    $('#step3').css('display', 'none');
    $('#bbuttons').css('display', 'none');
    $('#bsubmit').css('display', 'none');
}

/**
 * INIT
 */
$(window).on('load', function() {
    if (document.location.protocol == 'https:') {
        WIDGET_ROOT = WIDGET_ROOT.replace('http:', 'https:');
        SERVER_ROOT = SERVER_ROOT.replace('http:', 'https:');
    } else {
        WIDGET_ROOT = WIDGET_ROOT.replace('https:', 'http:');
        SERVER_ROOT = SERVER_ROOT.replace('https:', 'http:');
    }

    $('#bhelp').mouseover(function () {

        var x;

        if (($('#bhelp').position().left + 180) > $('#widget').width())
            x = $('#widget').width() - 200;
        else
            x = ($('#bhelp').position().left + 30) + 'px';

        $('#help').css('top', ($('#bhelp').position().top + $('#bhelp').height()) + 'px');
        $('#help').css('left', x + 'px');
        $('#help').css('display', 'block');
    });

    formDefault();
    mandatory('widgetform');

    if (WIDGET_MODE === 'staff-service') {
        $('#b').insertBefore($('#a'));
    }

    initPageCSS();


    //$('.help').tipTip({defaultPosition: 'right'});

    $('.bclose').on('click', function (e) {
        $.post(FRONT_ROOT + '/clear-session-value?mode=key&key=widgetMonth' + $('input[name="nonce"]').val(), '', function () {
            if (document.forms[0] && document.forms[0].r)
                window.location.href = document.forms[0].r.value;
            else if (document.getElementById('r'))
                window.location.href = document.getElementById('r').value;
            else
                parent.postMessage('close-scheduling', '*');
        });
    });

    var url = WIDGET_ROOT + '/info.jsp?' + getParameters() + '&page=' + $('.page').eq(0).attr('id') + '&mode=' + WIDGET_MODE;

    if ($('#start').val() != '')
        url += '&time=' + $('#start').val();

    if (window.location.href.indexOf('thank') == -1)
        $('.event-info').load(url);

    if ($('#id').length > 0) {
        _month();
        nav(0);
    }

    if ($('.group').length > 0) {

        $('#servicelist').find('li').css('display', 'none');
        $('#servicelist').find('li').removeClass('light');

        $('.group').on('click', function () {

            var selectedId = $(this).attr('id');

            $('.group').each(function (i) {

                if ($(this).attr('id') != selectedId) {

                    if ($(this).hasClass('opened')) {
                        $('#servicelist').find('li').css('display', 'none');
                        $(this).removeClass('opened');
                        $('#servicel').append($('#servicelist').detach());
                        $(this).find('i').removeClass('fa-angle-down');
                    }
                }
            });

            if (!$(this).hasClass('opened')) {

                $('.group').find('i').removeClass('fa-angle-down');

                $('#servicelist').find('li').css('display', 'none');
                var q = '' + $(this).data('services');
                var nb = q.split(',').length;
                var k = 0;

                $('#servicelist').find('li').each(function (i) {

                    $(this).css('border-bottom-color', '#313131');

                    if (q.search($(this).attr('id')) != -1) {
                        $(this).css('display', 'block');

                        k++;

                        if (k == nb)
                            $(this).css('border-bottom-color', 'white');
                    }
                });

                $('#g_' + $(this).attr('id')).append($('#servicelist').detach());
                $(this).find('i').addClass('fa-angle-down');
                $(this).addClass('opened');

                $('#servicelist').find('li')

            } else {
                $('#servicelist').find('li').css('display', 'none');
                $(this).removeClass('opened');
                $('#servicel').append($('#servicelist').detach());
                $(this).find('i').removeClass('fa-angle-down');

            }
        });
    }


    //Load month page if APPOINTMENT REQUEST mode without service and company list
    if (WIDGET_MODE === 'none' && $('#servicelist').length === 0 && $('#companylist').length === 0)
        _month();

    var eventIdLength =  $('#id').length;

    //If new event
    if ( eventIdLength === 0 ) {
        //Service Resource OR Service Mode : There is an External ID for the Service in the URL OR a service is already checked
        if (WIDGET_MODE === 'service' || WIDGET_MODE === 'service-staff' ) {
            if (getService() !== undefined) {
                _serviceChange(getService());
                ID_SERVICE_IN_URL = true ;
                $('#b-previous').css('display', 'none');
            }
        }

        if ($('#staff').length > 0 && WIDGET_MODE === 'staff-service') {
            _changeStaff();
        }

        //Resource Mode : There is an External ID for the Resource in the URL OR a resource is already checked
        if ( WIDGET_MODE === 'resource' ) {
            if (getResource() !== undefined) {
                _resourceChange(getResource());
                ID_RESSOURCE_IN_URL = true;
                $('#b-previous').css('display', 'none');

                if ($('#resources-options').length === 0) {
                    _month();
                } else {
                    $('#bbuttons').css('display', 'block');
                }
            }
        }

        if ($('#date').length > 0 &&  WIDGET_MODE == 'staff-service' && $('#date').is(':visible')) {
            _month();
        }
    }


    if ($('#date').length > 0 && $('#id').length == 0 && WIDGET_MODE == 'staff-service' && $('#date').is(':visible')) {
        _month();
    }

    $('#widget').css('display', 'block');
    resizeList();

    if ($('.visible').attr('ondisplay'))
        eval($('.visible').attr('ondisplay'));

    $('#widgetform').submit(function (event) {
        $('#submit-widget').prop('disabled', true);
    });

    $('#title').html($('.page').eq(0).data('title'));
    $('#subtitle').html($('.page').eq(0).data('subtitle'));

    if (typeof $('#timezone :selected').attr('data-sel') == 'undefined') {
        // get user time zone for tz selector
        var offset = -(new Date().getTimezoneOffset() / 60);
        var test = false;

        $('#timezone').find('option').each(function (i) {
            if (offset == eval($(this).attr('offset'))) {
                if ($(this).hasClass('managed')) {
                    $(this).attr('selected', 'selected');
                    test = true;
                }
            }
        });
    }

    if ($('.payment-box').length == 0) {
        $('#submit-widget').hide();
    }

    $('.payment-box').on('click', function () {
        $('#submit-widget').prop('disabled', false);
        $(this).addClass('payment-box-selected').siblings().removeClass('payment-box-selected');
        _payment()
    });

});

function resizeList() {
    $('.row').each(function () {

        var h = 50;

        $(this).find('div').each(function () {

            if ($(this).height() > h)
                h = $(this).height();
        });

        $(this).find('.items').find('div').each(function () {

            if (!$(this).hasClass('item') || $(this).hasClass('center')) {
                $(this).css('line-height', h + 'px');
                //alert($(this).html());
            }

        });
    });
}

function _changeHTML5Date(elm) {

    var s = '' + elm.value;

    var d = new Date(Date.parse(s));

    var ios = (navigator.userAgent.match(/like Mac OS X/i)) ? true : false;

    if (ios)
        d.setTime(d.getTime() + (3600 * 24 * 1000));

    setDates(null, d.getDate(), d.getMonth(), d.getFullYear(), true);
}

function showOption(max, maxIteration) {

    if (max === undefined && WIDGET_MODE != 'resource'){
        max = $('input[type=radio][name=service]:checked').val() === undefined ? MAX_COUNT : $('input[type=radio][name=service]:checked').data("count");
    }
    if (maxIteration === undefined && WIDGET_MODE != 'resource'){
        maxIteration = $('input[type=radio][name=service]:checked').val() === undefined ? MAX_ITERATION : ('input[type=radio][name=service]:checked').data("dailymax");
    }

    var objectCount = $('#rcount');
    var objectOptions = $("#rcount option");
    var rcountValue = parseInt($('#rcount').val());
    if(isNaN(rcountValue) || rcountValue > max){
        rcountValue = 1
    }

    var optionsDisplayed = 0;
    if (objectOptions.length > 1) {

        objectCount.find('option').removeAttr("selected");
        objectCount.find('option').removeAttr("disabled");

        var c2;
        objectOptions.each(function () {

            c2 = eval($(this).val());

            if (c2 > max) {
                $(this).css('display', 'none');
                $(this).attr('disabled', 'disabled');
            } else {
                $(this).css('display', 'block');
                optionsDisplayed++;
            }
        });

        if(objectOptions.length > rcountValue){
            $('#rcount option:nth-child('+rcountValue+')').attr("selected",true);
        }else{
            $('#rcount option:first-child').attr("selected",true);
        }

    }

    var divOptionCount = $("#object-option-count");
    var serviceR = ($('input[type=radio][name=service]:checked').val() === undefined ) ? SERVICE_RESOURCE_MODE : $('input[type=radio][name=service]:checked').data('serviceresource') ;
    var multiple = ($('input[type=radio][name=service]:checked').val() === undefined) ? MULTIPLE_BOOKING : $('input[type=radio][name=service]:checked').data('multiple') ;
    var iterationBooking = ($('input[type=radio][name=service]:checked').val() === undefined) ? ITERATION_BOOKING : $('input[type=radio][name=service]:checked').data('iteration')
    var overNight = $('input[type=radio][name=service]:checked').data('overnight') === undefined ? OVER_NIGHT : $('input[type=radio][name=service]:checked').data('overnight') ;
    if ( optionsDisplayed > 1 && (!serviceR || multiple)) {
        divOptionCount.show();
    } else {
        divOptionCount.hide();
    }

    //IN SERVICE RESOURCE MODE
    //hide the iterationOption in ServiceResource if the MultipleBooking is not allowed for the Service
    if (WIDGET_MODE !== 'resource') {
        if (!iterationBooking) {
            $('#object-option-iteration').hide();
        } else {
            $('#object-option-iteration').show();
            if (!overNight) {
                $('#riterations').find('option').each(function () {

                    if (maxIteration != 0) {
                        if (eval($(this).val()) > (maxIteration - 1)) {
                            $('#res-iteration-' + eval($(this).val())).prop("disabled", true);
                            $('#res-iteration-' + eval($(this).val())).hide();
                        } else {
                            $('#res-iteration-' + eval($(this).val())).show();
                            $('#res-iteration-' + eval($(this).val())).prop("disabled", false);
                        }
                    } else {
                        $('#res-iteration-' + eval($(this).val())).show();
                        $('#res-iteration-' + eval($(this).val())).prop("disabled", false);
                    }
                });
            }
        }
    }

    var serviceChecked ;
    if ( $('input[type=radio][name=service]:checked').data('iteration') === false || ( $('input[type=radio][name=service]:checked').data('iteration') === undefined && getService() === undefined)) {
        serviceChecked = false ;
    } else {
        serviceChecked = true ;
    }

    // La prochaine page est la page des options mais qu'on ne veut pas l'afficher
    var currentPage = $('.page').eq(getCurrentIndexPage());
    if ( (currentPage.attr('class').indexOf("options-page") > 0 || currentPage.attr('id') === 'services-options')
        // && divOptionCount.length !== 0 && (objectOptions.length === 0 || optionsDisplayed <= 1 )
        && WIDGET_MODE !== 'resource' && ($("#riterations option").length === 0 || !serviceChecked || (serviceChecked && !iterationBooking) )
        && ( (multiple && max === 1) || !multiple ))  {
        nextPage();
    }
    /**IN RESOURCE MODE :
     * Hide the options page :
     * 		if Recurrence AND MultipleBooking aren't allowed
     * 		if Recurrence isn't allowed AND Resource's capacity is 1 or less
     **/
    else if ( ( currentPage.attr('class').indexOf("options-page") > 0 ||currentPage.attr('id') === 'resources-options')
        && !serviceR
        && $("#riterations option").length === 0
        && (objectOptions.length === 0 || optionsDisplayed <= 1 )){
        nextPage();
    } else {
        if ( currentPage.attr("id") !== "date" ) {
            $("#b-next").show();
        }
    }

    if ( currentPage.attr('id') === "staffs" ) {
        $("#b-next").hide();
    }
}


function _showValidation() {

    $('#validation-message').html($('#validation-message').html().replace('[phone]', $('#phone').val()));

    $('#confirmation-code').on('keyup', function() {

        if ($(this).val().length == 7)
            $.post(WIDGET_ROOT + '/validation-check.jsp', {code: $(this).val()});

        /*
         if ($('#step4').attr('s') == $(this).val())
         $('#bsubmit').prop("disabled", false);
         */
    });

    $.post(WIDGET_ROOT + '/validation-post.jsp?' + getParameters() + '&time=' + $('#start').val(), {
            firstname: $('#firstname').val(),
            lastname: $('#firstname').val(),
            email: $('#email').val(),
            phone: $('#phone').val()
        }, function (data) {


        }
    );

}

function _previousValidation() {
    $('#step4').css('display', 'none');
    $('#step3').css('display', 'block');
    $('#bprev-validation').css('display', 'none');
    $('#bpdates').css('display', 'block');
    $('#bsubmit').css('display', 'none');
    $('#bvalidation').css('display', 'block');

    $('#validation-message').html($('#validation-message').html().replace($('#phone').val(), '[phone]'));
}

var countNew=0;
function _clientChange() {
    if ($('#client').val() == '1') {
        $('#existing-client').css('display', 'none');
        $('#new-client').css('display', 'block');
        countNew=1;
    }

}

function _changeTimeZone() {

    if ($('#dates').css('display') == 'block')
        _dates();
}

function _changeFormPage() {

    var arr = jQuery.makeArray($('.fields-page'));

    $('.fields-page').css('display', 'none');

    $(arr[FPAGE_INDEX]).css('display', 'block');

    $('#bpdates').css('display', 'none');

    if (FPAGE_INDEX == ($('.fields-page').length - 1)) {

        if (VALIDATION != 'none')
            $('#bvalidation').css('display', 'block');
        else
            $('#bsubmit').css('display', 'block');

        $('#bnextform').css('display', 'none');
    } else {
        $('#bsubmit').css('display', 'none');
        $('#bnextform').css('display', 'block');

    }

    if (FPAGE_INDEX == 0) {
        $('#bpdates').css('display', 'block');
        $('#bpreviousform').css('display', 'none');
        $('.standard-fields').css('display', 'block');
        $('#bnextform').css('display', 'block');
    } else {
        $('#bpreviousform').css('display', 'block');
        $('.standard-fields').css('display', 'none');


    }
}

function _nextForm() {

    FPAGE_INDEX++;

    _changeFormPage();
}

function _previousForm() {

    FPAGE_INDEX--;

    _changeFormPage();

}

function highlightStaff(id) {
    $('.staff').removeClass('staff-selected');
    $('#' + id).addClass('staff-selected');
}

function resourceMonth() {
    _month();
    nextPage();
    $('#b-next-action').css('display', 'none');
}

function nextPage() {
    if ($('#privacy-consent').is(':visible') && !$('#privacy-consent').is(':checked')) {
        $('#privacy-consent').addClass('check-mandatory');
        $('#privacy-consent-mandatory').show();
        return
    }

    if (waiting) {
        return
    }

    /*
     if ($('.page').eq(getCurrentIndexPage()).attr('id') == 'fields-email')
     _emailNext();
     else
     */

    nav(1);



}


function previousPage() {

    nav(-1);

    if (!$('#b-previous').is(':visible')) {
        // Retour sur premiÃ¨re page
        if (LOGIN_SERVICE != null && $('.login-box').is(':visible')) {
            $('#firstname').val('');
            $('#lastname').val('');
            $('#phone').val('');
            $('#email').val('');

            // TODO Logout connector
        }
    }

    // EXTERNAL ID VIA URL
    // Options Ã  afficher, quel que soit le mode (RESSOURCE OU SERVICE OU SERVICE-RESSOURCE)
    // La 1Ã¨re page est celle des options (apres selection du service normalement). On va cacher le bouton "prÃ©cedent" puisque que le service est choisi via URL
    if ( (ID_SERVICE_IN_URL || ID_RESSOURCE_IN_URL)
        && ( $('.page').eq(getCurrentIndexPage()).attr('id') === ('services-options') || $('.page').eq(getCurrentIndexPage()).hasClass('options-page'))
        && ( $('#object-option-count').css('display') !== 'none' ||  $('#object-option-iteration').css('display') !== 'none'
            || $('input[type=radio][name=service]:checked').data('iteration') || $('input[type=radio][name=service]:checked').data('multiple')
        )
    ) {
        $('#b-previous').css('display', 'none');
    }

    //SERVICE OU SERVICE-RESSOURCE : Options Ã  cacher et service-staff
    // la 1Ã¨re page est celle des staffs (apres selection du service normalement). On va cacher le bouton "prÃ©cedent" car le service est choisi via URL
    if (ID_SERVICE_IN_URL
        && WIDGET_MODE == 'service-staff'
        && ( $('.page').eq(getCurrentIndexPage()).attr('id') === ('staffs'))
        && ( ($('#object-option-count').css('display') !== 'block' &&  $('#object-option-iteration').css('display') !== 'block')
            || ( !($('input[type=radio][name=service]:checked').data('iteration') === undefined ? true : $('input[type=radio][name=service]:checked').data('iteration'))
                && !($('input[type=radio][name=service]:checked').data('multiple') == undefined ? true : $('input[type=radio][name=service]:checked').data('multiple'))
            )
        )
    ) {
        $('#b-previous').css('display', 'none');
    }

    //RESSOURCE : Options Ã  cacher
    // la 1Ã¨re page est celle du mois
    if (ID_RESSOURCE_IN_URL
        && WIDGET_MODE == 'resource'
        && ( $('.page').eq(getCurrentIndexPage()).attr('id') === ('resources'))
        && ( $('#object-option-count').css('display') !== 'none' ||  $('#object-option-iteration').css('display') !== 'none' || $('input[type=radio][name=service]:checked').data('iteration') || $('input[type=radio][name=service]:checked').data('multiple'))
    ) {
        $('#b-previous').css('display', 'none');
    }

}

function getCurrentIndexPage() {

    var index = 0;

    $('.page').each(function (i) {

        if ($(this).hasClass('visible'))
            index = i;
    });

    return index;
}


function validForm2(id, page){
    if ($('#privacy-consent').is(':visible') && !$('#privacy-consent').is(':checked')) {
        $('#privacy-consent').addClass('check-mandatory');
        $('#privacy-consent-mandatory').show();
        return
    }

    if (waiting) {
        return
    }

    var b = _validForm(id);



    var count=0;

    if(b) {
        if($("#new-client").css("display")=="none"){
            if($("#client").val()!=""){
                deleteAdditionalCookie();
                $('#' + id).submit();
            }
        }else{
            $('.field-date.field.required ').each(function() {
                if($(this).val() == ''){
                    count=1;
                    return false;
                }
            });
            if(count==0){
                deleteAdditionalCookie();
                $('#' + id).submit();
            }
        }
    }
}





var hasAdditionalFields = false;
var waiting = false;
function nav(inc) {
    var index = getCurrentIndexPage();
    var clientID=$("#client").val();


    if(clientID!=null&&clientID!="") {


        if ($('#new-client').css('display') != 'block') {
            loadAdditionnalField(clientID);
        }
        else if ($('#new-client').css('display') === 'block' && !$('#client-badge').length) {
            $(".bind_crm-address-firstline").val("");
            $(".bind_crm-address-secondline").val("");
            $(".bind_crm-address-city").val("");
            $(".bind_crm-address-postalcode").val("");
            $(".bind_crm-address-state").val("");
            $(".bind_crm-address-country").val("");
            $(".bind_crm-job-title").val("");
            $(".bind_crm-company").val("");
            $(".bind_crm-notifications-email").prop('checked', false);
            $(".bind_crm-notifications-sms").prop('checked', false);
            //$('.bind_crm-language option[value=""]').prop('selected', true);
            $(".bind_crm-reference").val("");
            $(".bind_crm-reminder-email").prop('checked', false);
            $(".bind_crm-reminder-sms").prop('checked', false);
            $(".bind_crm-miscellaneous-1").val("");
            $(".bind_crm-miscellaneous-2").val("");
            $(".bind_crm-miscellaneous-3").val("");
            $(".bind_crm-miscellaneous-4").val("");
            $(".bind_crm-miscellaneous-5").val("");
            $(".bind_crm-miscellaneous-6").val("");
            $(".bind_crm-miscellaneous-7").val("");
            $(".bind_crm-miscellaneous-8").val("");
            $(".bind_crm-miscellaneous-9").val("");
            $(".bind_crm-miscellaneous-10").val("");
        }

    }

    //alert(clientID);

    if (index == 0) {
        //hasAdditionalFields = $('.fields-page').length > 1;
        var fieldPage = 0;
        $('.fields-page').each(function (i, element) {
            if ($(element).find('.field').length !== 0) {
                fieldPage++;
            }
        });
        hasAdditionalFields = fieldPage > 1;
    }
    if ($('#rcount').length > 0 && $('#rcount').css('display') === 'block') {
        hasAdditionalFields = $('.fields-page').length > 2;
    }

    if (CUSTOM_ADD_FORM == true) {
        hasAdditionalFields = false;
    }

    var pageId = $('.page').eq(index).attr('id');

    var relationshipOtherSelected = ($('input[id="relationship-someone"]').prop("checked") === true);

    $('#submit-widget').css('display', 'none');
    $('#b-next').css('display', 'none');
    if ($('.page').eq(index).hasClass('fields-page') || $('.page').eq(index).hasClass('relationships-page')) {
        $('#b-next').css('display', 'inline');
        _activeRelationshipsNotif();

        if ((relationshipOtherSelected || hasAdditionalFields) && (index < $('.page').length - 1)) {

            $('#b-next').css('display', 'inline');

            $('#submit-widget').css('display', 'none');
        } else {

            $('#b-next').css('display', 'none');
            $('#submit-widget').css('display', 'inline');

        }
    }

    _activeContactFields();
    if (inc >= 0 && !_validForm('', $('.page').eq(index))) {
        return;
    }

    var backoffice = ($('#existing-client').length > 0);
    var newClientFormDisplayed = $('.page').eq(index).hasClass('fields-page') && ($('#new-client').css('display') === 'block');
    var newRelationshipsFormDisplayed = $('.page').eq(index).hasClass('relationships-page') && ($('#new-client-relationships').css('display') === 'block');

    //if previews on date page, clear date data
    if ( inc === -1 && pageId === 'date' ) {
        $('#month').val(CURRENT_MONTH);
        $('#year').val(CURRENT_YEAR);
        $('#day').val(CURRENT_DAY);
        $('#current-month').data('current-year',CURRENT_YEAR);
        $('#current-month').data('current-month',CURRENT_MONTH);
    }

    if (inc === 1 && $('.page').eq(index + 1).hasClass('relationships-page') && $('input[id="relationship-me"]').prop("checked")) {
        nav(2);
        return;
    } else if (inc === -1 && $('.page').eq(index - 1).hasClass('relationships-page') && $('input[id="relationship-me"]').prop("checked")) {
        // Ne pas afficher saisie relationship client
        nav(-2);
        return;
    } else if ( inc === -1
        && $('.page').eq(index - 1).hasClass('options-page')
        && $('#object-option-count').css('display') !== 'block' &&  $('#object-option-iteration').css('display') !== 'block'
    //&& !$('input[type=radio][name=service]:checked').data('serviceresource')
    ) {
        //Ne pas afficher la pages des options si elles sont masquees pour l'objet pour lequel nous voulons prendre rendez-vous
        nav(-2);
        return;
    }
    // IN SERVICE MODE ONLY
    if (inc === -1 && $('.page').eq(index - 1).attr('id') === 'services-options') {
        //Don't show the options page if IterationBooking AND MultipleBooking aren't Allowed in service-details.jsp
        if (!$('input[type=radio][name=service]:checked').data('iteration') && !$('input[type=radio][name=service]:checked').data('multiple')) {
            nav(-2);
            return;
        }
    }

    //Clear service filter with staff when go to previous page from staff pag
    if ( inc < 0 &&  $('.page').eq(index).attr('id') === 'staffs' && WIDGET_MODE === 'service-staff'  ) {
        //Some service group are availables
        if ( $('.group').length > 0 ) {
            $('#servicelist li').css('display', 'none');
            if ( $('.group.opened').length !== 0 ) {
                var servicesIds = $('.group.opened').data('services').split(",");
                servicesIds.forEach( function(elm,index) {
                    $('#servicelist li#' + elm).css('display', 'block');
                });
            }
        } else {
            $('#servicelist li').css('display', 'block');
        }
    }

    if (inc > 0 && (newClientFormDisplayed && !_validForm('', $('.page').eq(index))) && $('#client').val() === '') {
        return;
    }

    if (inc > 0 && $('.page').eq(index).hasClass('fields-page') && $('#client').length > 0 && $('#client').val() == '' && $('#new-client').css('display') != 'block') {
        return;
    }

    if (inc > 0 && (newRelationshipsFormDisplayed && !_validForm('', $('.page').eq(index))) && $('#client-relationships').val() === '') {
        return;
    }

    if (inc > 0 && $('.page').eq(index).hasClass('relationships-page') && $('#client-relationships').length > 0 && $('#client-relationships').val() == '' && $('#new-client-relationships').css('display') != 'block')
        return;

    if (inc === -1 && $('.page').eq(index).hasClass('fields-page') && backoffice && newClientFormDisplayed) {
        // Retour sur ecran de recherche client
        $('#existing-client').css('display', 'block');
        $('#new-client').css('display', 'none');
        $('#confirm-block').css('display', 'none');
        if($('#client-relationships').length > 0){
            $('#relationships-takefor-header').css('display', 'block');
            //return;
        }
    }


    $('.page').css('display', 'none');
    $('.page').eq(index).removeClass('visible');

    index += inc;

    if ($('.page').eq(index).hasClass('fields-page') && $('.page').eq(index).find('.field').length === 0 ) {
        index += inc;
    }

    if (index < 0)
        index = 0;

    pageId = $('.page').eq(index).attr('id');

    if (index != $('.page').length) {
        $('.page').eq(index).css('display', 'block');
        $('#title').html($('.page').eq(index).data('title'));
        $('#subtitle').html($('.page').eq(index).data('subtitle'));
        $('.page').eq(index).addClass('visible');
    }

    updateNav(index);

    if (pageId === 'staffs' && $('#id').length > 0) {
        $('#staff').remove();
    }

    if (pageId === 'services' && $('#id').length > 0) {
        $('#service').remove();
    }

    if (pageId === 'resources' && $('#id').length > 0) {
        $('#resource').remove();
    }

    if (pageId === 'date' && inc !== -1) {
        _month();
        $('#b-next').css('display', 'none');
    }

    var url = WIDGET_ROOT + '/info.jsp?' + getParameters() + '&page=' + $('.page').eq(index).attr('id') + '&mode=' + WIDGET_MODE;

    if ($('#start').val() !== '')
        url += '&time=' + $('#start').val();

    $('.event-info').load(url);

    if (index <= 0)
        $('#b-previous').css('display', 'none');
    else
        $('#b-previous').css('display', 'inline');

    $('#bbuttons').css('display', 'block');

    if (pageId === 'resources-options' ) {
        //$('#b-next-action').css('display', 'inline');

        if ($('#' + getResource() + '-duration').length > 0)
            $('.d').html('x ' + $('#' + getResource() + '-duration').html());
        else
            $('.d').html('');
    }

    if (pageId === 'date' && INDEX === NONE_COUNT ) {
        $('#b-next').css('display', 'inline');
    } else if ($('.page').eq(getCurrentIndexPage()).attr('id') === 'date'){
        $('#b-next').css('display', 'none');
    }

    if (pageId === 'validation-sms') {
        _showValidation();
    }

    if ($('.page').eq(index).attr('ondisplay'))
        eval($('.page').eq(index).attr('ondisplay'));

    if (pageId === 'services' && WIDGET_MODE === 'none')
        $('.choicedate').click();

    $('#back-companies').css('display', (index === 0) ? 'inline' : 'none');

    var relationshipOtherSelected = ($('input[id="relationship-someone"]').prop("checked") === true);

    $('#submit-widget').css('display', 'none');
    $('#b-next').css('display',pageId === 'services-options' || pageId === 'resources-options'? 'inline' : 'none');

    if ($('.page').eq(index).hasClass('fields-page') || $('.page').eq(index).hasClass('relationships-page')) {
        $('#b-next').css('display', 'inline');
        _activeRelationshipsNotif();

        if ((relationshipOtherSelected || hasAdditionalFields) && (index < $('.page').length - 1)) {

            $('#b-next').css('display', 'inline');

            $('#submit-widget').css('display', 'none');
        } else {

            $('#b-next').css('display', 'none');
            $('#submit-widget').css('display', 'inline');

        }
    }

    if (pageId !== 'resources-options') {
        $('#b-next-action').css('display', 'none');
    }

    if (pageId === 'fields') {
        if ($('input[type=radio][name=service]:checked').data('payable')) {
            $("#mandatory-btn-confirm").hide();
            $("#mandatory-btn-next").show();
        } else {
            $("#mandatory-btn-confirm").show();
            $("#mandatory-btn-next").hide();
        }
    }

    if ($('.page').eq(index).hasClass('relationships-page')) {
        _activeRelationshipsNotif();
    }
    resizeList();
}

function updateNav(index) {
    // nav bullets
    $('.nav-box').css('color', '#FFF');
    $('.nav-box').removeClass('done');

    $('.nav-circles i').removeClass('fa-circle');
    $('.nav-circles i').addClass('fa-circle-thin');

    for (var p = 0; p < index + 1; p++) {
        $('.nav-box').eq(p).addClass('done');
        $('.nav-circles i').eq(p).removeClass('fa-circle-thin');
        $('.nav-circles i').eq(p).addClass('fa-circle');
    }
}

function requiredToMandatory(){
    var required = $('.required');
    $('.required').addClass('mandatory');

}

function mandatory(id) {

    //alert($$('.mandatory').length);

    //var form = document.forms[forname];

    var m = $('.mandatory:not([type="checkbox"]):not([type="radio"])');
    if (m.length > 0) {
        $(id).submit(function (event) {

            //alert(validForm(id));
        });
    }


    var ok = true;

    $('.mand').remove();

    m.each(function (j) {

        var v = $(document.createElement('span'));


        v.html('*');
        v.addClass('mand');

        //$(this).after(v);

        var windowWidth = window.screen.width < window.outerWidth ? window.screen.width : window.outerWidth;

        if (windowWidth < 500){
            var label = $(this).parent().find('label');
            if(!label.html().endsWith(' *') && label.find(".info-mand").length === 0){
                $(this).parent().find('label').html($(this).parent().find('label').html() + ' <span class="info-mand">*</span>');
            }
        }
        else
            $(this).after(v);

    });
}

function _showConfirmation() {

    var url = WIDGET_ROOT + '/confirm-date.jsp?' + getParameters() + '&mode=' + WIDGET_MODE;

    if ($('#start').val() != '')
        url += '&time=' + $('#start').val();

    $('#new-date').load(url);
    if ($('#old-date').text() === "")
        $('#old-date').html($('#old').val());
}

function _showResourceOptions() {
    $('#b-next-action').css('display', 'inline');
}

function _emailNext() {

    $.get(WIDGET_ROOT + '/consumer?fn=pwd&c=' + $('#company').val() + '&cc=' + $('#email-consumer').val() + '&p=' + $('#password-consumer').val(), function (data) {

        if (data.clientId) {
            if (data.clientId != 0) {

                var field = $(document.createElement('input'));
                field.attr('type', 'hidden');
                field.attr('name', 'client');
                field.attr('id', 'client');
                field.val(data.clientId);

                $('#widgetform').append(field);

                if ($('.form-group').length > 0)
                    nav(2);
                else {
                    validForm('widgetform');
                }
            }
        }
        else
            $('#consumer-error').html('No Consumer Account');

    });
    /*
     if (!$('#email-consumer').hasClass('validated')) {

     $('#email-consumer').addClass('field-load');

     $.get(WIDGET_ROOT + '/consumer?fn=email&c=' + $('#company').val() + '&cc=' + $('#email-consumer').val(), function (data) {

     if (!data.isConsumer) {
     $('#email').val($('#email-consumer').val());
     nav(1);
     } else {

     var field = $(document.createElement('input'));
     field.attr('type', 'hidden');
     field.attr('name', 'client');
     field.attr('id', 'client');
     field.val(data.clientId);

     $('#widgetform').append(field);

     $('#email-consumer').addClass('validated');
     $('#password').css('display', 'inline-block');
     $('#email-consumer').removeClass('field-load');
     }
     });
     } else {

     $.get(WIDGET_ROOT + '/consumer?fn=pwd&c=' + $('#company').val() + '&cc=' + $('#email-consumer').val() + '&p=' + $('#password-consumer').val(), function (data) {

     if (data.clientId != 0) {

     if ($('.form-group').length > 0)
     nav(2);
     else {
     validForm('widgetform');
     }
     }
     });
     }
     */
}

function _resourceGroupChange(groupId) {

    var members = '' + $('#' + groupId).data('members');

    $('#servicelist').find('li').each(function (i) {
        if (members.indexOf($(this).attr('id')) != -1)
            $(this).css('display', 'block');
        else
            $(this).css('display', 'none');
    });

    nextPage(1);
}

function _payment() {
    var selectedPayments = $('.payment-box-selected');
    if (selectedPayments.length == 1) {
        var url = $('#payment-url', $(selectedPayments[0]).closest("div.payment-box")).val();
        if(url.indexOf("monetary") !== -1){
            window.top.location.href = url;
        }else{
            window.open(url);
        }
    }
}

function _showLogin() {
    $('.event-info').hide();
    $('#bbuttons').hide();
    $('.page, .identity-page').css('height', $(window).height() - 130);
}

function _login(service, companyId) {
    LOGIN_SERVICE = service;
    var redirect = WIDGET_ROOT + "/end-service.jsp";
    var loginWindow = window.open(FRONT_ROOT + "/connector/login/" + service + "?scope=init&companyId=" + companyId + "&redirect=" + redirect, "login", "width=600,height=600");
}

function _loginSuccess() {
    $('.event-info').show();
    $('#bbuttons').show();
    $('.page, .identity-page').css('height', $(window).height() - 205);

    nextPage(1);

    if ($('.page').eq(getCurrentIndexPage()).attr('id') == 'date') {
        _month();
    }
}

function updateDate(year, month, day, time) {
    document.getElementById("year").value = year;
    document.getElementById("month").value = month;
    document.getElementById("day").value = day;
    document.getElementById("start").value = time;

    $('#widgetform').submit();
}

function _activeRelationships() {
    nav(0);
}

function _activeRelationshipsNotif() {
    if ($('#relationship-me').prop("checked")) {
        $('#relationship-firstname').removeClass('mandatory');
        $('#relationship-lastname').removeClass('mandatory');
        $('#relationship-email').removeClass('mandatory');
        $('#relationship-phone').removeClass('mandatory');
    } else if ($('#new-client-relationships').css('display') === 'block') {
        $('#relationship-firstname').addClass('mandatory');
        $('#relationship-lastname').addClass('mandatory');
        if ($('input[id="relationships-for-me"]').prop("checked")) {
            if ($('#relationship-email').parent().find('.mand').length) {
                $('#relationship-email').removeClass('mandatory');
                $('#relationship-email').removeClass('backoffice-mandatory');
                $('#relationship-email').parent().find('.mand').hide();
            }

            if ($('#relationship-phone').parent().find('.mand').length) {
                $('#relationship-phone').removeClass('mandatory');
                $('#relationship-phone').removeClass('backoffice-mandatory');
                $('#relationship-phone').parent().find('.mand').hide();
            }

            if ($('#relationship-email').parent().find('.info-mand').length){
                var label = $('#relationship-email').parent().find('label').html();
                $('#relationship-email').removeClass('mandatory');
                $('#relationship-email').parent().find('.info-mand').hide();
            }

            if ($('#relationship-phone').parent().find('.info-mand').length){
                var label = $('#relationship-phone').parent().find('label').html();
                $('#relationship-phone').removeClass('mandatory');
                $('#relationship-phone').parent().find('.info-mand').hide();
            }
        } else {
            if ($('#relationship-email').parent().find('.mand').length) {
                $('#relationship-email').addClass('mandatory');
                $('#relationship-email').addClass('backoffice-mandatory');
                $('#relationship-email').parent().find('.mand').show();
            } else {
                $('#relationship-email').removeClass('mandatory');
                $('#relationship-email').removeClass('backoffice-mandatory');
                $('#relationship-email').parent().find('.mand').hide();
            }

            if ($('#relationship-phone').parent().find('.mand').length) {
                $('#relationship-phone').addClass('mandatory');
                $('#relationship-phone').addClass('backoffice-mandatory');
                $('#relationship-phone').parent().find('.mand').show();
            } else {
                $('#relationship-phone').removeClass('mandatory');
                $('#relationship-phone').removeClass('backoffice-mandatory');
                $('#relationship-phone').parent().find('.mand').hide();
            }

            if ($('#relationship-email').parent().find('.info-mand').length){
                var label = $('#relationship-email').parent().find('label').html();
                $('#relationship-email').addClass('mandatory');
                $('#relationship-email').parent().find('.info-mand').show();
            }

            if ($('#relationship-phone').parent().find('.info-mand').length){
                var label = $('#relationship-phone').parent().find('label').html();
                $('#relationship-phone').addClass('mandatory');
                $('#relationship-phone').parent().find('.info-mand').show();
            }
        }
    }
}

function _activeContactFields() {
    /*$('#firstname').removeClass('mandatory');
    $('#lastname').removeClass('mandatory');
    $('#phone').removeClass('mandatory');
    $('#email').removeClass('mandatory');

    if ($('#new-client').css('display') === 'block') {
        $('#firstname').addClass('mandatory');
        $('#lastname').addClass('mandatory');
        if ($('#email').parent().find('.mand').length) {
            $('#email').addClass('mandatory');
            $('#email').addClass('backoffice-mandatory');
            $('#email').parent().find('.mand').show();
        }

        if ($('#phone').parent().find('.mand').length) {
            $('#phone').addClass('mandatory');
            $('#phone').addClass('backoffice-mandatory');
            $('#phone').parent().find('.mand').show();
        }
    }*/
}


/*function getTestFieldPage(index){
 //If ( current page is Field Page and current page is Last page ) OR ( Next page is field page and next page is last page and no field in next page ) OR (Current page is relationship) OR (Next page is relationships page and for me)
 console.log( $('input[id="relationship.takefor.me"]').prop( "checked") === true );
 return ( $('.page').eq(index).hasClass('fields-page') && index === $('.page').length - 1 ) ||
 ( $('.page').eq(index+1).hasClass('fields-page') && index + 1 === $('.page').length - 1 && $('.page').eq(index+1).find('.field').length === 0 ) ||
 ( $('.page').eq(index).hasClass('relationships-page')) ||
 ( $('.page').eq(index+1).hasClass('relationships-page') && $('input[id="rel  ationship-me"]').prop( "checked") === true );
 }*/

function showService(service) {
    window.open(WIDGET_ROOT + "/save-" + service + "?init=true&companyId=" + $('#company').val(), "service", "width=400,height=400");
}

function sendGoogleAnalyticsEvent(category,action,label) {
    /*ga('send', {
        hitType : 'event',
        eventCategory: category,
        eventAction : action,
        eventLabel : label
    });
    if ($('input[name="gid"]').val() !== undefined && $('input[name="gid"]').val() !== "") {
        ga('AZUserGa.send', {
            hitType : 'event',
            eventCategory: category,
            eventAction : action,
            eventLabel : label
        });
    }*/

}

function initPageCSS() {
    $('.page, .identity-page').css('height', $(window).height() - 205);
    $('.page').css('overflow-y', 'auto');
}

function loadAdditionnalField(clientID){
    $.ajax({
        method: 'GET',
        url: WIDGET_ROOT + '/client-information.jsp',
        data: 'clientId=' + clientID,
        async:true,
        beforeSend: function(){
            $(document.body).css("cursor","wait");
            $("#submit-widget").css("cursor","wait");

            waiting = true;
        },
        complete: function(){

            $(document.body).css("cursor","pointer");
            $("#submit-widget").css("cursor","pointer");

            waiting = false;
        },
        success: function (data) {

            var json = JSON.parse(data);

            if(json.addresse1!=""){
                $(".bind_crm-address-firstline").val(json.addresse1);
            }else if($(".bind_crm-address-firstline").val()!=""){
                $(".bind_crm-address-firstline").val($(".bind_crm-address-firstline").val());
            }else{
                $(".bind_crm-address-firstline").val("");
            }

            if(json.addresse2!=""){
                $(".bind_crm-address-secondline").val(json.addresse2);
            }else if($(".bind_crm-address-secondline").val()!=""){
                $(".bind_crm-address-secondline").val($(".bind_crm-address-secondline").val());
            }else{
                $(".bind_crm-address-secondline").val("");
            }

            if(json.city!=""){
                $(".bind_crm-address-city").val(json.city);
            }else if($(".bind_crm-address-city").val()!=""){
                $(".bind_crm-address-city").val($(".bind_crm-address-city").val());
            }else{
                $(".bind_crm-address-city").val("");
            }

            if(json.zipcode!=""){
                $(".bind_crm-address-postalcode").val(json.zipcode);
            }else if($(".bind_crm-address-postalcode").val()!=""){
                $(".bind_crm-address-postalcode").val($(".bind_crm-address-postalcode").val());
            }else{
                $(".bind_crm-address-postalcode").val("");
            }

            if(json.state!=""){
                $(".bind_crm-address-state").val(json.state);
            }else if($(".bind_crm-address-state").val()!=""){
                $(".bind_crm-address-state").val($(".bind_crm-address-state").val());
            }else{
                $(".bind_crm-address-state").val("");
            }

            if(json.country!=""){
                $(".bind_crm-address-country").val(json.country);
            }else if($(".bind_crm-address-country").val()!=""){
                $(".bind_crm-address-country").val($(".bind_crm-address-country").val());
            }else{
                $(".bind_crm-address-country").val("");
            }

            if(json.jobTitle!=""){
                $(".bind_crm-job-title").val(json.jobTitle);
            }else if($(".bind_crm-job-title").val()!=""){
                $(".bind_crm-job-title").val($(".bind_crm-job-title").val());
            }else{
                $(".bind_crm-job-title").val("");
            }

            if(json.company!=""){
                $(".bind_crm-company").val(json.company);
            }else if($(".bind_crm-company").val()!=""){
                $(".bind_crm-company").val($(".bind_crm-company").val());
            }else{
                $(".bind_crm-company").val("");
            }


            if (json.notification == "all"&&($(".bind_crm-notifications-email").attr('checked')!=null&&$(".bind_crm-notifications-sms").attr('checked')!=null)) {
                $(".bind_crm-notifications-email").prop('checked', true);
                $(".bind_crm-notifications-sms").prop('checked', true);
            }
            else if (json.notification == "email"&&$(".bind_crm-notifications-email").attr('checked')==null) {
                $(".bind_crm-notifications-email").prop('checked', true);
            }
            else if (json.notification == "sms"&&$(".bind_crm-notifications-sms").attr('checked')==null) {
                $(".bind_crm-notifications-sms").prop('checked', true);
            }
            else if($(".bind_crm-notifications-email").attr('checked')!=null){
                $(".bind_crm-notifications-email").prop('checked', true);
            }
            else if($(".bind_crm-notifications-email").attr('checked')==null){
                $(".bind_crm-notifications-email").prop('checked', false);
            }
            else if($(".bind_crm-notifications-sms").attr('checked')!=null){
                $(".bind_crm-notifications-sms").prop('checked', true);
            }
            else if($(".bind_crm-notifications-sms").attr('checked')==null){
                $(".bind_crm-notifications-sms").prop('checked', false);
            }
            else if (json.notification == ""||($(".bind_crm-notifications-email").attr('checked')==null&&$(".bind_crm-notifications-sms").attr('checked')==null)) {
                $(".bind_crm-notifications-email").prop('checked', false);
                $(".bind_crm-notifications-sms").prop('checked', false);
            }

            if(json.reference!=""){
                $(".bind_crm-reference").val(json.reference);
            }else if($(".bind_crm-reference").val()!=""){
                $(".bind_crm-reference").val($(".bind_crm-reference").val());
            }else{
                $(".bind_crm-reference").val("");
            }

            if (json.reminder == "all"&&($(".bind_crm-reminder-email").attr('checked')!=null&&$(".bind_crm-reminder-sms").attr('checked')!=null)) {
                $(".bind_crm-reminder-email").prop('checked', true);
                $(".bind_crm-reminder-sms").prop('checked', true);
            }
            else if (json.reminder == "email"&&$(".bind_crm-reminder-email").attr('checked')==null) {
                $(".bind_crm-reminder-email").prop('checked', true);
            }
            else if (json.reminder == "sms"&&$(".bind_crm-reminder-sms").attr('checked')==null) {
                $(".bind_crm-reminder-sms").prop('checked', true);
            }
            else if($(".bind_crm-reminder-email").attr('checked')!=null){
                $(".bind_crm-reminder-email").prop('checked', true);
            }
            else if($(".bind_crm-reminder-email").attr('checked')==null){
                $(".bind_crm-reminder-email").prop('checked', false);
            }
            else if($(".bind_crm-reminder-sms").attr('checked')!=null){
                $(".bind_crm-reminder-sms").prop('checked', true);
            }
            else if($(".bind_crm-reminder-sms").attr('checked')==null){
                $(".bind_crm-reminder-sms").prop('checked', false);
            }
            else if (json.reminder == ""||($(".bind_crm-reminder-email").attr('checked')==null&&$(".bind_crm-reminder-sms").attr('checked')==null)) {
                $(".bind_crm-reminder-email").prop('checked', false);
                $(".bind_crm-reminder-sms").prop('checked', false);
            }



            if (json.misc1 != "") {
                $(".bind_crm-miscellaneous-1").val(json.misc1);
            }else if ($(".bind_crm-miscellaneous-1").val() != "") {
                $(".bind_crm-miscellaneous-1").val($(".bind_crm-miscellaneous-1").val());
            }else {
                $(".bind_crm-miscellaneous-1").val("");
            }

            if (json.misc2 != "") {
                $(".bind_crm-miscellaneous-2").val(json.misc2);
            }else if ($(".bind_crm-miscellaneous-2").val() != "") {
                $(".bind_crm-miscellaneous-2").val($(".bind_crm-miscellaneous-2").val());
            }else {
                $(".bind_crm-miscellaneous-2").val("");
            }

            if (json.misc3 != "") {
                $(".bind_crm-miscellaneous-3").val(json.misc3);
            }else if ($(".bind_crm-miscellaneous-3").val() != "") {
                $(".bind_crm-miscellaneous-3").val($(".bind_crm-miscellaneous-3").val());
            }else {
                $(".bind_crm-miscellaneous-3").val("");
            }

            if (json.misc4 != "") {
                $(".bind_crm-miscellaneous-4").val(json.misc4);
            }else if ($(".bind_crm-miscellaneous-4").val() != "") {
                $(".bind_crm-miscellaneous-4").val($(".bind_crm-miscellaneous-4").val());
            }else {
                $(".bind_crm-miscellaneous-4").val("");
            }

            if (json.misc5 != "") {
                $(".bind_crm-miscellaneous-5").val(json.misc5);
            }else if ($(".bind_crm-miscellaneous-5").val() != "") {
                $(".bind_crm-miscellaneous-5").val($(".bind_crm-miscellaneous-5").val());
            }else {
                $(".bind_crm-miscellaneous-5").val("");
            }

            if (json.misc6 != "") {
                $(".bind_crm-miscellaneous-6").val(json.misc6);
            }else if ($(".bind_crm-miscellaneous-6").val() != "") {
                $(".bind_crm-miscellaneous-6").val($(".bind_crm-miscellaneous-6").val());
            }else {
                $(".bind_crm-miscellaneous-6").val("");
            }

            if (json.misc7 != "") {
                $(".bind_crm-miscellaneous-7").val(json.misc7);
            }else if ($(".bind_crm-miscellaneous-7").val() != "") {
                $(".bind_crm-miscellaneous-7").val($(".bind_crm-miscellaneous-7").val());
            }else {
                $(".bind_crm-miscellaneous-7").val("");
            }

            if (json.misc8 != "") {
                $(".bind_crm-miscellaneous-8").val(json.misc8);
            }else if ($(".bind_crm-miscellaneous-8").val() != "") {
                $(".bind_crm-miscellaneous-8").val($(".bind_crm-miscellaneous-8").val());
            }else {
                $(".bind_crm-miscellaneous-8").val("");
            }

            if (json.misc9 != "") {
                $(".bind_crm-miscellaneous-9").val(json.misc9);
            }else if ($(".bind_crm-miscellaneous-9").val() != "") {
                $(".bind_crm-miscellaneous-9").val($(".bind_crm-miscellaneous-9").val());
            }else {
                $(".bind_crm-miscellaneous-9").val("");
            }

            if (json.misc10 != "") {
                $(".bind_crm-miscellaneous-10").val(json.misc10);
            }else if ($(".bind_crm-miscellaneous-10").val() != "") {
                $(".bind_crm-miscellaneous-10").val($(".bind_crm-miscellaneous-10").val());
            }else {
                $(".bind_crm-miscellaneous-10").val("");
            }


        }
    });
}

function deleteAdditionalCookie() {
    var name;
    if (document.cookie && document.cookie != '') {
        var split = document.cookie.split(';');
        for (var i = 0; i < split.length; i++) {
            name = split[i].split("=")[0].trim();
            if(name.startsWith("bind_") || name.startsWith("field_")){
                document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
            }
        }
    }
}

function testConsent(){
    if ($('#privacy-consent').is(':visible') && !$('#privacy-consent').is(':checked')) {
        $('#privacy-consent').addClass('check-mandatory');
        $('#privacy-consent-mandatory').show();
        return
    }
}
