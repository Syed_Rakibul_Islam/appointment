<?php

return [


    'thank-you' => [
        'top_section' => '<b>Thank you!</b><br/> You\'ve scheduled an appointment for <b>[[service]]</b> with <b>[[operator]]</b> on <b>[[date]]</b> at <b>[[time]]</b>. We have sent you a confirmation email.',
    ],
    'confirm' => [
        'header' => 'Confirmation',
        'top_section' => '<b>Hi [[first_name]],</b> <br/> I\'m writing to let you know that your appointment with [[operator]] on [[date]] at [[time]] has been reviewed and confirmed.',
        'main_section' => '[[service]] on [[date]] at [[time]]',
        'bottom_section' => 'If you would like to modify or cancel this appointment please use the button below.<br><br/>See you soon'
    ],
    'reschedule' => [
        'header' => 'Rescheduling',
        'top_section' => '<b>Hi [[first_name]],</b> <br/> Your appointment with [[operator]] has been modified. This appointment is now scheduled for ....<br>',
        'main_section' => '[[service]] on [[date]] at [[time]]',
        'bottom_section' => 'If you would like to modify or cancel this appointment please use the button below.<br><br/>See you soon'
    ],
    'cancel' => [
        'header' => 'Cancellation',
        'top_section' => '<b>Dear [[first_name]],</b> <br/>This email confirms your requested cancellation of your appointment with [[operator]] at [[time]] on [[date]]. I hope we can reschedule at your convenience.<br></p><p>See you soon</p>',
        'main_section' => '',
        'bottom_section' => ''
    ],
    'reminder' => [
        'header' => 'Reminder',
        'top_section' => '<b>Hi [[first_name]],</b> <br/> I\'m writing to remind you of your upcoming appointment.<br>',
        'main_section' => '[[service]] on [[date]] at [[time]]',
        'bottom_section' => 'If you would like to modify or cancel this appointment please use the button below.<br><br/>See you soon'
    ],


];
