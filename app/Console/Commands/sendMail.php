<?php

namespace App\Console\Commands;

use App\Appointment;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendAppointment;

class sendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $appointments = Appointment::whereNull('remind')->whereDate('schedule', '<=', Carbon::now()->addHours(24)->format('Y-m-d'))->get();
        foreach($appointments as $appointment){
            if(!empty($appointment->email)) {
                $this->sendMarkdownMail($appointment, 'appointment-remainder', $appointment->email, false, 'Reminder');
            }
            $appointment->update(['remind' => 1]);
        }
    }
    private function sendMarkdownMail($appointment, $path, $mailTo, $isOrg = false, $type = 'Confirmation'){
        if($isOrg){
            $mailFromName = env('MAIL_FROM_NAME');
        }
        else{
            $mailFromName = @$appointment->service->user->org_name && !empty($appointment->service->user->org_name) ? $appointment->service->user->org_name : env('MAIL_FROM_NAME');
        }
        try{
            Mail::to($mailTo)->send(new SendAppointment(
                @$appointment->service->user->org_name,
                $appointment->f_name . ' ' . $appointment->l_name,
                @$appointment->service->name,
                @$appointment->operator->name,
                Carbon::parse($appointment->schedule)->format('M d, Y'),
                Carbon::parse($appointment->schedule)->format('h:i A'),
                $isOrg ? url('appointments/' . $appointment->uuid ) : url('book/' . $appointment->uuid . '/change'),
                $path,
                $mailTo,
                env('MAIL_FROM_ADDRESS'),
                $mailFromName,
                $type,
                @$appointment->service->user->website
            ));
        }
        catch(Exception $e){
            logger($e);
        }
    }
}
