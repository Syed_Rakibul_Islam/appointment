<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOperator extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'service_id', 'operator_id'
    ];

    public function seervice(){
        return $this->belongsTo('App\Service');
    }
    public function operator(){
        return $this->belongsTo('App\Operator');
    }
}
