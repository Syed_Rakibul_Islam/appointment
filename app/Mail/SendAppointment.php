<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAppointment extends Mailable
{
    use Queueable, SerializesModels;
    private $org_name, $name, $service, $operator, $date, $time, $url, $path, $mailTo, $mailFrom, $mailFromName, $type, $websie;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($org_name, $name, $service, $operator, $date, $time, $url, $path, $mailTo, $mailFrom, $mailFromName, $type, $websie)
    {
        $this->org_name = $org_name;
        $this->name = $name;
        $this->service = $service;
        $this->operator = $operator;
        $this->date = $date;
        $this->time = $time;
        $this->url = $url;
        $this->path = $path;
        $this->mailTo = $mailTo;
        $this->mailFrom = $mailFrom;
        $this->mailFromName = $mailFromName;
        $this->type = $type;
        $this->websie = $websie;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.' . $this->path)
            ->from($this->mailFrom, $this->mailFromName)
            ->replyTo($this->mailTo, ucfirst($this->type))
            ->subject('Appointment ' . $this->type)
            ->with([
                'org_name' => !empty($this->org_name) ? $this->org_name : config('app.name'),
                'name' => $this->name,
                'service' => $this->service,
                'operator' => $this->operator,
                'date' => $this->date,
                'time' => $this->time,
                'url' => $this->url,
                'website' => !empty($this->websie) ? $this->websie : url('/')
            ]);
    }
}
