<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = [
        'name', 'header', 'top_section', 'main_section', 'bottom_section', 'options', 'user_id'
    ];
}
