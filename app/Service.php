<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name', 'duration', 'image', 'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function operators(){
        return $this->belongsToMany('App\Operator', 'service_operators');
    }
}
