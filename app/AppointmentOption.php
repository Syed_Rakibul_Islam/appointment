<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentOption extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'appointment_id', 'option_id'
    ];

    public function appointment(){
        return $this->belongsTo('App\Appointment');
    }
    public function option(){
        return $this->belongsTo('App\Option');
    }
}
