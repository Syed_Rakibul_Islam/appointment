<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = [
        'uuid', 'service_id', 'operator_id', 'schedule', 'for', 'f_name', 'l_name', 'email', 'phone', 'status', 'remind'
    ];

    public function service(){
        return $this->belongsTo('App\Service');
    }
    public function operator(){
        return $this->belongsTo('App\Operator');
    }

    public function options(){
        return $this->belongsToMany('App\Option','appointment_options');
    }
}
