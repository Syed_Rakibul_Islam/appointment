<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'name', 'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function appointments(){
        return $this->belongsToMany('App\Appointment','appointment_options');
    }
}
