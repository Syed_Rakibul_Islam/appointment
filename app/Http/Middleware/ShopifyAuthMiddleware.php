<?php

namespace App\Http\Middleware;

use Closure;

class ShopifyAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ( empty($user) || (isset($user->uninstall_date) && !empty($user->uninstall_date)) ) {
            return redirect('/');
        }

        return $next($request);
    }
}
