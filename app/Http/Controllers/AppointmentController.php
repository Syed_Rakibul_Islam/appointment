<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Libraries\ShopifyClient;
use App\Mail\SendAppointment;
use App\Message;
use App\Operator;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Uuid;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index($uuid){
        $user = User::where('uuid', $uuid)->with('services', 'options')->firstOrFail();
        $shopifyClient = new ShopifyClient($user->name, $user->token, env('SHOPIFY_APP_KEY'), env('SHOPIFY_APP_SECRET'));
        try{
            $recurringCharges = $shopifyClient->call('GET', '/admin/recurring_application_charges/'. $user->charge_id .'.json');
            if(isset($recurringCharges['status']) && $recurringCharges['status'] == 'active'){
                return view('appointment.index', compact('user'));
            }
        }
        catch(\Exception $e){
            logger($e);
        }
        return view('appointment.expired', compact('user'));
    }
    public function store(Request $request){
        $this->validate($request, [
            'service_id' => 'required',
            'operator_id' => 'required',
            'appointment_date' => 'required',
            'appointment_time' => 'required',
            'for' => 'nullable:max:20',
            'f_name' => 'required|max:100',
            'l_name' => 'required|max:100',
            'email' => 'required|max:150|email',
            'phone' => 'required|max:250',
            'checkbox' => 'nullable|array'
        ]);

        $appointment = new Appointment();
        $appointment->uuid = Uuid::generate();
        $appointment->service_id = $request->service_id;
        $appointment->operator_id = $request->operator_id;
        $appointment->schedule = Carbon::parse($request->appointment_date . ' ' . $request->appointment_time)->format('Y-m-d H:i:s');
        $appointment->for = $request->for;
        $appointment->f_name = $request->f_name;
        $appointment->l_name = $request->l_name;
        $appointment->email = $request->email;
        $appointment->phone = $request->phone;
        $appointment->save();

        if(!empty($request->checkbox)){
            $appointment->options()->attach($request->checkbox);
        }
        if(!empty($appointment->email)) {
            $this->sendMarkdownMail($appointment, 'appointment', $appointment->email, false, 'Confirmation');
        }
        if(!empty(@$appointment->service->user->org_email)) {
            $this->sendMarkdownMail($appointment, 'appointment-notify', $appointment->service->user->org_email, true, 'Confirmation');
        }

        return redirect('book/' . $appointment->uuid . '/thank-you');
    }
    public function thankYou($uuid){
        $appointment = Appointment::where('uuid', $uuid)->with('service.user', 'operator', 'options')->firstOrFail();
        $message = Message::where(['name' => 'thank-you', 'user_id' => @$appointment->service->user->id])->first();
        return view('appointment.thank-you', compact('appointment', 'message'));
    }
    public function print($uuid){
        $appointment = Appointment::where('uuid', $uuid)->with('service.user', 'operator', 'options')->firstOrFail();
        return view('appointment.print', compact('appointment'));
    }
    public function change($uuid){
        $appointment = Appointment::where('uuid', $uuid)->with('service.user', 'operator', 'options')->firstOrFail();
        if($appointment->status == 'cancel'){
            return redirect('book/' . $appointment->uuid . '/declined');
        }
        return view('appointment.change', compact('appointment'));
    }
    public function edit($uuid){
        $appointment = Appointment::where('uuid', $uuid)->with('service.user', 'operator', 'options')->firstOrFail();
        return view('appointment.edit', compact('appointment'));
    }
    public function iframeEdit($uuid){
        $appointment = Appointment::where('uuid', $uuid)->with('service.user', 'operator', 'options')->firstOrFail();
        $name = @$appointment->service->name;
        $duration = @$appointment->service->duration;
        $timezone = 'Europe/London';
        $dateTime = Carbon::now()->setTimezone($timezone);
        $today = $dateTime;

        $nextMonth = false;

        $user = @$appointment->service->user;
        $opening = json_decode(@$user->opening);


        return view('appointment.iframe-edit', compact( 'appointment', 'user', 'name', 'dateTime', 'nextMonth', 'duration', 'today', 'opening'));
    }
    public function update($uuid, Request $request){
        $appointment = Appointment::where('uuid', $uuid)->firstOrFail();
        $this->validate($request, [
            'appointment_date' => 'required',
            'appointment_time' => 'required'
        ]);

        $appointment->schedule = Carbon::parse($request->appointment_date . ' ' . $request->appointment_time)->format('Y-m-d H:i:s');
        $appointment->save();

        if(!empty($appointment->email)) {
            $this->sendMarkdownMail($appointment, 'appointment-reschedule', $appointment->email, false, 'Rescheduling');
        }
        if(!empty(@$appointment->service->user->org_email)) {
            $this->sendMarkdownMail($appointment, 'appointment-reschedule-notify', $appointment->service->user->org_email, true, 'Rescheduling');
        }

        return redirect('book/' . $appointment->uuid . '/thank-you');
    }
    public function cancel($uuid){
        $appointment = Appointment::where('uuid', $uuid)->firstOrFail();
        if($appointment->status == 'cancel'){
            return redirect('book/' . $appointment->uuid . '/declined');
        }
        $appointment->update(['status' => 'cancel']);

        if(!empty($appointment->email)) {
            $this->sendMarkdownMail($appointment, 'appointment-cancel', $appointment->email, false, 'Cancellation');
        }
        if(!empty(@$appointment->service->user->org_email)) {
            $this->sendMarkdownMail($appointment, 'appointment-cancel-notify', $appointment->service->user->org_email, true, 'Cancellation');
        }

        return redirect('book/' . $appointment->uuid . '/canceled');
    }
    public function declined($uuid){
        $appointment = Appointment::where('uuid', $uuid)->with('service.user', 'operator', 'options')->firstOrFail();
        if($appointment->status != 'cancel'){
            return redirect('book/' . $appointment->uuid . '/change');
        }
        return view('appointment.decline', compact('appointment'));
    }
    public function canceled($uuid){
        $appointment = Appointment::where(['uuid' => $uuid, 'status' => 'cancel'])->with('service.user', 'operator', 'options')->firstOrFail();
        if($appointment->status != 'cancel'){
            return redirect('book/' . $appointment->uuid . '/change');
        }
        return view('appointment.canceled', compact('appointment'));
    }
    public function operators($id, Request $request){
        $name = $request->name;
        $operators = Operator::whereHas('services', function($query) use($id) {
            $query->where('service_id', '=', $id);
        })->get();
        return view('appointment.operators', compact('name', 'operators'));
    }
    public function schedules($id, Request $request){
        $name = $request->name;
        $duration = $request->duration;
        $timezone = !empty($request->timezone) ? $request->timezone : 'Europe/London';
        $dateTime = Carbon::now()->setTimezone($timezone);
        $today = $dateTime;

        $user = User::where('uuid', $request->uuid)->select('opening')->firstOrFail();
        $opening = json_decode($user->opening);

        $nextMonth = false;

        return view('appointment.schedules', compact('name', 'dateTime', 'nextMonth', 'duration', 'today', 'opening'));
    }
    public function changeMonth(Request $request){
        $duration = $request->duration;
        $dateTime = Carbon::parse($request->date);
        $timezone = !empty($request->timezone) ? $request->timezone : 'Europe/London';
        $nextMonth = Carbon::parse($request->date)->format('Y-m') > Carbon::now()->setTimezone($timezone)->format('Y-m') ? true : false;
        $today = Carbon::now()->setTimezone($timezone);

        $user = User::where('uuid', $request->uuid)->select('opening')->firstOrFail();
        $opening = json_decode($user->opening);

        return view('appointment.months', compact('dateTime', 'nextMonth', 'duration', 'today', 'opening'));
    }

    public function changeDate(Request $request){
        $duration = $request->duration;
        $dateTime = Carbon::parse($request->date);
        $timezone = !empty($request->timezone) ? $request->timezone : 'Europe/London';
        $today = Carbon::now()->setTimezone($timezone);

        $user = User::where('uuid', $request->uuid)->select('opening')->firstOrFail();
        $opening = json_decode($user->opening);

        return view('appointment.times', compact('dateTime', 'duration', 'today', 'opening'));
    }

    private function sendMarkdownMail($appointment, $path, $mailTo, $isOrg = false, $type = 'Confirmation'){
        if($isOrg){
            $mailFromName = env('MAIL_FROM_NAME');
        }
        else{
            $mailFromName = @$appointment->service->user->org_name && !empty($appointment->service->user->org_name) ? $appointment->service->user->org_name : env('MAIL_FROM_NAME');
        }
        try{
            Mail::to($mailTo)->send(new SendAppointment(
                @$appointment->service->user->org_name,
                $appointment->f_name . ' ' . $appointment->l_name,
                @$appointment->service->name,
                @$appointment->operator->name,
                Carbon::parse($appointment->schedule)->format('M d, Y'),
                Carbon::parse($appointment->schedule)->format('h:i A'),
                $isOrg ? url('appointments/' . $appointment->uuid ) : url('book/' . $appointment->uuid . '/change'),
                $path,
                $mailTo,
                env('MAIL_FROM_ADDRESS'),
                $mailFromName,
                $type,
                @$appointment->service->user->website
            ));
        }
        catch(Exception $e){
            logger($e);
        }
    }
}
