<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Mail\SendAppointment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
class UserAppointment extends Controller
{
    public function index(){
        $appointments = Appointment::whereHas('service', function($q) { $q->where('user_id', auth()->user()->id); })->with('service.user', 'operator')->paginate(20);
        return view('user_appointment.index', compact('appointments'));

    }
    public function show($id){
        $appointment = Appointment::whereHas('service', function($q) { $q->where('user_id', auth()->user()->id); })->with('service.user', 'operator', 'options')->findOrFail($id);
        return view('user_appointment.show', compact('appointment'));
    }
    public function ban($id){
        $appointment = Appointment::whereHas('service', function($q) { $q->where('user_id', auth()->user()->id); })->with('service.user', 'operator', 'options')->findOrFail($id);


        if($appointment->status != 'cancel'){
            $appointment->update(['status' => 'cancel']);

            if(!empty($appointment->email)) {
                $this->sendMarkdownMail($appointment, 'admin-appointment-cancel', $appointment->email, false, 'Cancellation');
            }

            Session::flash('success', 'Appointment canceled successfully!');
        }




        return redirect('appointments');

        return view('user_appointment.show', compact('appointment'));
    }
    public function view($uuid){
        $appointment = Appointment::where('uuid', $uuid)->whereHas('service', function($q) { $q->where('user_id', auth()->user()->id); })->with('service.user', 'operator', 'options')->firstOrFail();
        return view('user_appointment.show', compact('appointment'));
    }

    private function sendMarkdownMail($appointment, $path, $mailTo, $isOrg = false, $type = 'Confirmation'){
        try{
            Mail::to($mailTo)->send(new SendAppointment(
                @$appointment->service->user->org_name,
                $appointment->f_name . ' ' . $appointment->l_name,
                @$appointment->service->name,
                @$appointment->operator->name,
                Carbon::parse($appointment->schedule)->format('M d, Y'),
                Carbon::parse($appointment->schedule)->format('h:i A'),
                $isOrg ? url('appointments/' . $appointment->uuid ) : url('book/' . $appointment->uuid . '/change'),
                $path,
                $mailTo,
                @$appointment->service->user->org_email && !empty($appointment->service->user->org_email) ? $appointment->service->user->org_email : env('MAIL_FROM_ADDRESS'),
                @$appointment->service->user->org_name && !empty($appointment->service->user->org_name) ? $appointment->service->user->org_name : env('MAIL_FROM_NAME'),
                $type,
                @$appointment->service->user->website
            ));
        }
        catch(Exception $e){
            logger($e);
        }
    }
}
