<?php

namespace App\Http\Controllers;

use App\Option;
use Illuminate\Http\Request;
use Session;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $options = Option::where('user_id', auth()->user()->id)->latest()->paginate(20);
        return view('option.index', compact('options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('option.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:150'
        ]);
        $request->request->add(['user_id' => auth()->user()->id]);

        Option::create($request->all());

        Session::flash('success', 'Option added successfully!');

        return redirect('options');
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $option = Option::where('user_id', auth()->user()->id)->findOrFail($id);
        return view('option.show', compact('option'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $option = Option::where('user_id', auth()->user()->id)->findOrFail($id);
        return view('option.edit', compact('option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $option = Option::where('user_id', auth()->user()->id)->findOrFail($id);
        $this->validate($request, [
            'name' => 'required|max:150'
        ]);

        $option->update($request->all());
        Session::flash('success', 'Option updated successfully!');

        return redirect('options');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $option = Option::where('user_id', auth()->user()->id)->findOrFail($id);
        $option->delete();

        Session::flash('success', 'Option deleted successfully!');

        return redirect('options');
    }
}
