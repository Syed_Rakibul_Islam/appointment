<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function jsonData(){
        $appointments = Appointment::whereHas('service', function($q) { $q->where('user_id', auth()->user()->id); })->whereNull('status')->with('service.user', 'operator')->get();
        $calanderData = [];
        $color = 1;
        foreach($appointments as $appointment){
            $calanderData[] = [
                'name' => $appointment->f_name . ' ' . $appointment->l_name,
                'date' => Carbon::parse($appointment->schedule)->format('d'),
                'month' => Carbon::parse($appointment->schedule)->format('m'),
                'year' => Carbon::parse($appointment->schedule)->format('Y'),
                'start_time' => Carbon::parse($appointment->schedule)->format('H:i'),
                'end_time' => Carbon::parse($appointment->schedule)->addMinutes(@$appointment->service->duration)->format('H:i'),
                'description' => "<a href='" . url('appointments/' . $appointment->uuid) . "' target='_blank' > Appointment with " . @$appointment->operator->name . " for " . @$appointment->service->name . " </a>",
                'color' => $color
            ];
            $color = $color == 4 ? 1 : $color;
            $color++;
        }
        echo json_encode($calanderData);

    }

    public function updateSettings(Request $request){

        $this->validate($request, [
            'org_name' => 'required|max:150',
            'org_email' => 'nullable|max:150',
            'contact' => 'nullable|max:150',
            'organization_image' => 'nullable|max:2000',
            'address' => 'nullable|max:255',
            'location' => 'nullable|url|max:255',
            'website' => 'nullable|url|max:255',
            'description' => 'nullable|max:1000',
        ]);

        $user = User::findOrFail(auth()->user()->id);
        if ($request->file('organization_image')){
            $temp = $request->file('organization_image');
            $path = 'images/upload/organization/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);


            if(isset($user->image) && !empty($user->image) && file_exists(($path . $user->image))){
                unlink($path . $user->image);
            }
            $user->image = $full_name;
        }

        $user->org_name = $request->org_name;
        $user->org_email = $request->email;
        $user->contact = $request->contact;
        $user->address = $request->address;
        $user->location = $request->location;
        $user->website = $request->website;
        $user->description = $request->description;
        $user->save();
        Session::flash('success', 'User settings updated successfully!');

        return redirect('settings');
    }
    public function updateOpening(Request $request){

        $this->validate($request, [
            'sunday' => 'required',
            'monday' => 'required',
            'tuesday' => 'required',
            'wednesday' => 'required',
            'thursday' => 'required',
            'friday' => 'required',
            'saturday' => 'required'
        ]);

        $opening = [];
        $opening['sunday']['status'] = $request->sunday;
        if($request->sunday == 'open'){
            $this->validate($request, [
                'sunday_morning_start' => 'required|date_format:g:i A',
                'sunday_morning_end' => 'required:date_format:g:i A',
                'sunday_afternoon_start' => 'required|date_format:g:i A',
                'sunday_afternoon_end' => 'required:date_format:g:i A',
            ]);
            $opening['sunday']['morning']['start'] = $request->sunday_morning_start;
            $opening['sunday']['morning']['end'] = $request->sunday_morning_end;
            $opening['sunday']['afternoon']['start'] = $request->sunday_afternoon_start;
            $opening['sunday']['afternoon']['end'] = $request->sunday_afternoon_end;
        }
        $opening['monday']['status'] = $request->monday;
        if($request->monday == 'open'){
            $this->validate($request, [
                'monday_morning_start' => 'required|date_format:g:i A',
                'monday_morning_end' => 'required:date_format:g:i A',
                'monday_afternoon_start' => 'required|date_format:g:i A',
                'monday_afternoon_end' => 'required:date_format:g:i A',
            ]);
            $opening['monday']['morning']['start'] = $request->monday_morning_start;
            $opening['monday']['morning']['end'] = $request->monday_morning_end;
            $opening['monday']['afternoon']['start'] = $request->monday_afternoon_start;
            $opening['monday']['afternoon']['end'] = $request->monday_afternoon_end;
        }
        $opening['tuesday']['status'] = $request->tuesday;
        if($request->tuesday == 'open'){
            $this->validate($request, [
                'tuesday_morning_start' => 'required|date_format:g:i A',
                'tuesday_morning_end' => 'required:date_format:g:i A',
                'tuesday_afternoon_start' => 'required|date_format:g:i A',
                'tuesday_afternoon_end' => 'required:date_format:g:i A',
            ]);
            $opening['tuesday']['morning']['start'] = $request->tuesday_morning_start;
            $opening['tuesday']['morning']['end'] = $request->tuesday_morning_end;
            $opening['tuesday']['afternoon']['start'] = $request->tuesday_afternoon_start;
            $opening['tuesday']['afternoon']['end'] = $request->tuesday_afternoon_end;
        }
        $opening['wednesday']['status'] = $request->wednesday;
        if($request->wednesday == 'open'){
            $this->validate($request, [
                'wednesday_morning_start' => 'required|date_format:g:i A',
                'wednesday_morning_end' => 'required:date_format:g:i A',
                'wednesday_afternoon_start' => 'required|date_format:g:i A',
                'wednesday_afternoon_end' => 'required:date_format:g:i A',
            ]);
            $opening['wednesday']['morning']['start'] = $request->wednesday_morning_start;
            $opening['wednesday']['morning']['end'] = $request->wednesday_morning_end;
            $opening['wednesday']['afternoon']['start'] = $request->wednesday_afternoon_start;
            $opening['wednesday']['afternoon']['end'] = $request->wednesday_afternoon_end;
        }
        $opening['thursday']['status'] = $request->thursday;
        if($request->thursday == 'open'){
            $this->validate($request, [
                'thursday_morning_start' => 'required|date_format:g:i A',
                'thursday_morning_end' => 'required:date_format:g:i A',
                'thursday_afternoon_start' => 'required|date_format:g:i A',
                'thursday_afternoon_end' => 'required:date_format:g:i A',
            ]);
            $opening['thursday']['morning']['start'] = $request->thursday_morning_start;
            $opening['thursday']['morning']['end'] = $request->thursday_morning_end;
            $opening['thursday']['afternoon']['start'] = $request->thursday_afternoon_start;
            $opening['thursday']['afternoon']['end'] = $request->thursday_afternoon_end;
        }
        $opening['friday']['status'] = $request->friday;
        if($request->friday == 'open'){
            $this->validate($request, [
                'friday_morning_start' => 'required|date_format:g:i A',
                'friday_morning_end' => 'required:date_format:g:i A',
                'friday_afternoon_start' => 'required|date_format:g:i A',
                'friday_afternoon_end' => 'required:date_format:g:i A',
            ]);
            $opening['friday']['morning']['start'] = $request->friday_morning_start;
            $opening['friday']['morning']['end'] = $request->friday_morning_end;
            $opening['friday']['afternoon']['start'] = $request->friday_afternoon_start;
            $opening['friday']['afternoon']['end'] = $request->friday_afternoon_end;
        }
        $opening['saturday']['status'] = $request->saturday;
        if($request->saturday == 'open'){
            $this->validate($request, [
                'saturday_morning_start' => 'required|date_format:g:i A',
                'saturday_morning_end' => 'required:date_format:g:i A',
                'saturday_afternoon_start' => 'required|date_format:g:i A',
                'saturday_afternoon_end' => 'required:date_format:g:i A',
            ]);
            $opening['saturday']['morning']['start'] = $request->saturday_morning_start;
            $opening['saturday']['morning']['end'] = $request->saturday_morning_end;
            $opening['saturday']['afternoon']['start'] = $request->saturday_afternoon_start;
            $opening['saturday']['afternoon']['end'] = $request->saturday_afternoon_end;
        }

        $user = User::findOrFail(auth()->user()->id);
        $user->opening = json_encode($opening);
        $user->save();

        Session::flash('success', 'Opening hours updated successfully!');

        return redirect('settings');

    }
}
