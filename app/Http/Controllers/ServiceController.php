<?php

namespace App\Http\Controllers;

use App\Operator;
use App\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::where('user_id', auth()->user()->id)->latest()->paginate(20);
        return view('service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $operators = Operator::where('user_id', auth()->user()->id)->select('id', 'name')->get();
        return view('service.create', compact('operators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:150',
            'duration' => 'required|numeric|min:5|max:500',
            'feature_image' => 'nullable|max:255',
            'operators' => 'nullable|array',
        ]);
        $request->request->add(['user_id' => auth()->user()->id]);

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/service/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);
        }

        $service = Service::create($request->all());
        if(!empty($request->operators)){
            $service->operators()->attach($request->operators);
        }
        Session::flash('success', 'Service added successfully!');

        return redirect('services');
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::where('user_id', auth()->user()->id)->findOrFail($id);
        return view('service.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $operators = Operator::where('user_id', auth()->user()->id)->get();
        $service = Service::where('user_id', auth()->user()->id)->with('operators')->findOrFail($id);
        $serviceOperators = [];
        foreach($service->operators as $operator)
        {
            $serviceOperators[] = $operator->id;
        }

        return view('service.edit', compact('service', 'operators', 'serviceOperators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::where('user_id', auth()->user()->id)->findOrFail($id);
        $this->validate($request, [
            'name' => 'required|max:150',
            'duration' => 'required|numeric|min:5|max:500',
            'feature_image' => 'nullable|max:255'
        ]);

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/service/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($service->image) && file_exists(($path . $service->image))){
                unlink($path . $service->image);
            }
        }
        $service->update($request->all());
        if(!empty($request->operators)){
            $service->operators()->sync($request->operators);
        }
        Session::flash('success', 'Service updated successfully!');

        return redirect('services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::where('user_id', auth()->user()->id)->findOrFail($id);

        $path = 'images/upload/service/';
        if(!empty($service->image) && file_exists(($path . $service->image))){
            unlink($path . $service->image);
        }
        $service->operators()->detach();

        $service->delete();

        Session::flash('success', 'Service deleted successfully!');

        return redirect('services');
    }
}
