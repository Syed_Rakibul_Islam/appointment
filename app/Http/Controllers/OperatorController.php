<?php

namespace App\Http\Controllers;

use App\Operator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class OperatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operators = Operator::where('user_id', auth()->user()->id)->latest()->paginate(20);
        return view('operator.index', compact('operators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('operator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:150',
            'feature_image' => 'nullable|max:255'
        ]);
        $request->request->add(['user_id' => auth()->user()->id]);

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/operator/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);
        }

        Operator::create($request->all());
        Session::flash('success', 'Operator added successfully!');

        return redirect('operators');
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $operator = Operator::where('user_id', auth()->user()->id)->findOrFail($id);
        return view('operator.show', compact('operator'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $operator = Operator::where('user_id', auth()->user()->id)->findOrFail($id);
        return view('operator.edit', compact('operator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $operator = Operator::where('user_id', auth()->user()->id)->findOrFail($id);
        $this->validate($request, [
            'name' => 'required|max:150',
            'feature_image' => 'nullable|max:255'
        ]);

        if ($request->file('feature_image')){
            $temp = $request->file('feature_image');
            $path = 'images/upload/operator/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($operator->image) && file_exists(($path . $operator->image))){
                unlink($path . $operator->image);
            }
        }

        $operator->update($request->all());
        Session::flash('success', 'Operator updated successfully!');

        return redirect('operators');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $operator = Operator::where('user_id', auth()->user()->id)->findOrFail($id);
        $operator->services()->detach();
        $path = 'images/upload/operator/';
        if(!empty($operator->image) && file_exists(($path . $operator->image))){
            unlink($path . $operator->image);
        }
        $operator->delete();

        Session::flash('success', 'Operator deleted successfully!');

        return redirect('operators');

    }
}
