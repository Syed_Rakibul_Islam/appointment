<?php

namespace App\Http\Controllers;

use Auth;
use App\Libraries\ShopifyClient;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Session;
use Uuid;

class ShopifyAppController extends Controller
{
    public function index(){
        return view('install');
    }
    public function install(Request $request){

        $scopes = 'unauthenticated_read_content';
        $redirectUrl = url('/oauth');

        $redirect = 'https://' . $request->shop . '/admin/oauth/authorize?client_id=' . env('SHOPIFY_APP_KEY') . '&redirect_uri=' . $redirectUrl . '&state=strong_nonce&scope=' . $scopes;

        return redirect($redirect);

    }
    public function oauth(Request $request){
        if(isset($request->hmac) && isset($request->shop) && $this->verifyHmac($request->hmac) && $request->code){

            $shopifyClient = new ShopifyClient($request->shop, "", env('SHOPIFY_APP_KEY'), env('SHOPIFY_APP_SECRET'));

            try{
                $accessToken = $shopifyClient->getAccessToken($request->code);
            }
            catch (\Exception $e){
                logger($e);
                Session::flash('warning', 'Get access token error. Try again!');
                return redirect('/');
            }
            $shopifyClient = new ShopifyClient($request->shop, $accessToken, env('SHOPIFY_APP_KEY'), env('SHOPIFY_APP_SECRET'));

            /*Recuring charge*/
            $recurring = [
                'recurring_application_charge' =>
                    [
                        'name' => config('app.name'),
                        'price' => 9.99,
                        'return_url' => url('/charge?shop=' . $request->shop),
                        'trial_days' => 7,
                        'test' => true
                    ]
            ];
            $redirectUrl = '';
            $chargeId = 0;
            $shopifyApp = User::where('name', $request->shop)->first();
            $isInstallAgain = false;
            if(!empty($shopifyApp)) {
                if(!empty($shopifyApp->charge_id)){
                    $recurringCharges = [];
                    try{
                        $recurringCharges = $shopifyClient->call('GET', '/admin/recurring_application_charges/'. $shopifyApp->charge_id .'.json');
                    }
                    catch(\Exception $e){
                        logger($e);
                    }
                    if (!empty($recurringCharges)) {
                        if (isset($recurringCharges['status']) && $recurringCharges['status'] == 'pending' && isset($recurringCharges['confirmation_url']) && is_string($recurringCharges['confirmation_url']) && $recurringCharges['confirmation_url'] !== '') {
                            $redirectUrl = $recurringCharges['confirmation_url'];
                            $chargeId = $recurringCharges['id'];
                        }
                        elseif (isset($recurringCharges['status']) && $recurringCharges['status'] == 'accepted' && isset($recurringCharges['decorated_return_url'])) {
                            $redirectUrl = $recurringCharges['decorated_return_url'];
                            $chargeId = $recurringCharges['id'];
                        }
                        elseif (isset($recurringCharges['status']) && $recurringCharges['status'] == 'active') {
                            $redirectUrl = '/dashboard';
                            $chargeId = $recurringCharges['id'];
                        }
                        elseif(isset($recurringCharges['status']) && ($recurringCharges['status'] == 'cancelled' || $recurringCharges['status'] == 'expired') ){
                            if(!empty($shopifyApp->recuring_days))$recurringDay = $shopifyApp->recuring_days + 1;
                            else $recurringDay = 0;

                            $trialEndDate = (new Carbon)->parse($shopifyApp->install_date)->addDays($recurringDay);
                            $today = Carbon::now();

                            if($trialEndDate > $today){
                                $recurring['recurring_application_charge']['trial_days'] = $trialEndDate->diffInDays($today);
                            }
                            else{
                                $recurring['recurring_application_charge']['trial_days'] = 0;
                            }

                            /* Create recurring charge*/
                            try{
                                $createRecurringCharges = $shopifyClient->call('POST', '/admin/recurring_application_charges.json', $recurring);
                            }
                            catch(\Exception $e){
                                logger($e);
                                logger('Create recurring error.');
                                return redirect('/');
                            }

                            $redirectUrl = $createRecurringCharges['confirmation_url'];
                            $chargeId = $createRecurringCharges['id'];
                            $isInstallAgain = true;
                        }
                        elseif (isset($recurringCharges['status']) && $recurringCharges['status'] == 'declined') {
                            $redirectUrl = '/declined';
                            $chargeId = $recurringCharges['id'];
                        }
                        else {
                            Session::flash('warning', 'Verification failed! Recurring status: ' . $recurringCharges['status']);
                            return redirect('/');
                        }
                    }
                    else{
                        if(!empty($shopifyApp->recuring_days))$recurringDay = $shopifyApp->recuring_days + 1;
                        else $recurringDay = 0;

                        $trialEndDate = (new Carbon)->parse($shopifyApp->install_date)->addDays($recurringDay);
                        $today = Carbon::now();

                        if($trialEndDate > $today){
                            $recurring['recurring_application_charge']['trial_days'] = $trialEndDate->diffInDays($today);
                        }
                        else{
                            $recurring['recurring_application_charge']['trial_days'] = 0;
                        }

                        /* Create recurring charge*/
                        try{
                            $createRecurringCharges = $shopifyClient->call('POST', '/admin/recurring_application_charges.json', $recurring);
                        }
                        catch(\Exception $e){
                            logger($e);
                            logger('Create recurring error.');
                            return redirect('/');
                        }

                        $redirectUrl = $createRecurringCharges['confirmation_url'];
                        $chargeId = $createRecurringCharges['id'];
                        $isInstallAgain = true;
                    }
                }
                else{
                    if(!empty($shopifyApp->recuring_days))$recurringDay = $shopifyApp->recuring_days + 1;
                    else $recurringDay = 0;

                    $trialEndDate = (new Carbon)->parse($shopifyApp->install_date)->addDays($recurringDay);
                    $today = Carbon::now();

                    if($trialEndDate > $today){
                        $recurring['recurring_application_charge']['trial_days'] = $trialEndDate->diffInDays($today);
                    }
                    else{
                        $recurring['recurring_application_charge']['trial_days'] = 0;
                    }

                    /* Create recurring charge*/
                    try{
                        $createRecurringCharges = $shopifyClient->call('POST', '/admin/recurring_application_charges.json', $recurring);
                    }
                    catch(\Exception $e){
                        logger($e);
                        logger('Create recurring error.');
                        return redirect('/');
                    }

                    $redirectUrl = $createRecurringCharges['confirmation_url'];
                    $chargeId = $createRecurringCharges['id'];
                    $isInstallAgain = true;
                }
            }
            else{
                /* Create recurring charge*/
                try{
                    $createRecurringCharges = $shopifyClient->call('POST', '/admin/recurring_application_charges.json', $recurring);
                }
                catch(\Exception $e){
                    logger($e);
                    logger('Create recurring error.');
                    return redirect('/');
                }

                $redirectUrl = $createRecurringCharges['confirmation_url'];
                $chargeId = $createRecurringCharges['id'];
            }

            if(!empty($shopifyApp)){
                if($shopifyApp->token != $accessToken || $shopifyApp->charge_id != $chargeId){
                    $shopifyApp->token = $accessToken;
                    $shopifyApp->uninstall_date = null;
                    $shopifyApp->charge_id = '' . $chargeId;
                }
                if($isInstallAgain == true){
                    $shopifyApp->install_date = Carbon::now();
                }
                $shopifyApp->save();
                $shop = $shopifyApp;
            }
            else{
                $shop = User::create([
                    'uuid' => Uuid::generate(),
                    'name' => $request->shop,
                    'token' => $accessToken,
                    'charge_id' => '' . $chargeId,
                    'install_date' => Carbon::now(),
                    'recuring_days' => $recurring['recurring_application_charge']['trial_days']
                ]);
            }
            $request->session()->flush('shop');
            $request->session()->put('shop', ['id' => $shop->id, 'name' => $shop->name, 'token' => $accessToken]);


            if (!empty($redirectUrl)){
                if(@$shopifyApp->id){
                    Auth::loginUsingId($shopifyApp->id);
                }
                return redirect()->to($redirectUrl);
            };
            Auth::loginUsingId($shopifyApp->id);
            return redirect('/dashboard');
        }
    }
    public function charge(Request $request){
        if (isset($request->shop) && isset($request->charge_id)){
            $shopifyApp = User::where('name', '=', $request->shop)->where('charge_id', '=', $request->charge_id)->first();
            if(!empty($shopifyApp)){

                /*App recurring charges*/
                $shopifyClient = new ShopifyClient($shopifyApp->name, $shopifyApp->token, env('SHOPIFY_API_KEY'), env('SHOPIFY_SECRET_KEY'));

                /*Recurring error*/
                try{
                    $recurringCharges = $shopifyClient->call('GET', '/admin/recurring_application_charges/'. $shopifyApp->charge_id .'.json');
                }
                catch (\Exception $e){
                    logger($e);
                    Session::flash('warning', 'Recurring API error. Try again please!');
                    return redirect('/');
                }

                if (isset($recurringCharges['status']) && $recurringCharges['status'] == 'accepted') {
                    try{
                        $shopifyClient->call('POST', '/admin/recurring_application_charges/'.$shopifyApp->charge_id.'/activate.json');

                        /* Check uninstall webhook */
                        $showWebhooks = $shopifyClient->call('GET', '/admin/webhooks.json');
                        if (!empty($showWebhooks)) {
                            foreach ($showWebhooks as $showWebhook) {
                                if (isset($showWebhook['topic']) && ($showWebhook['topic'] == 'app/uninstalled')) {
                                    $shopifyClient->call('DELETE', '/admin/webhooks/' . $showWebhook['id'] . '.json');
                                }
                            }
                        }
                        $webhook = [
                            'webhook' =>
                                [
                                    'topic' => 'app/uninstalled',
                                    'address' => url('/api/uninstall'),
                                    'format' => 'json'
                                ]
                        ];
                        try{
//                            $shopifyClient->call('POST', '/admin/webhooks.json', $webhook);
                        }
                        catch (\Exception $e){
                            logger($e);
                            Session::flash('warning', 'Post webhook data error. Try again!');
                            return redirect('/');
                        }


                        Auth::loginUsingId($shopifyApp->id);
                        return redirect('/dashboard');
                    }
                    catch (\Exception $e){
                        logger($e);
                        Session::flash('warning', 'Recurring API post error. Try again please!');
                        return redirect('/');
                    }
                }
                elseif (isset($recurringCharges['status']) && $recurringCharges['status'] == 'declined') {
                    Session::flash('warning', 'Recurring declined!');
                    return redirect('/');
                }
            }
        }
        Session::flash('warning', 'Recurring verification error! ' . $request->charge_id);
        return redirect('/');
    }
    public function uninstall(Request $request){

        function verify_webhook($data, $hmac_header) {
            $calculated_hmac = base64_encode(hash_hmac('sha256', $data, env('SHOPIFY_SECRET_KEY'), true));
            return ($hmac_header == $calculated_hmac);
        }
        $hmac_header = $request->header('X-Shopify-Hmac-Sha256');
        $data = file_get_contents('php://input');
        $verified = verify_webhook($data, $hmac_header);

        if(var_export($verified, true)){
            $shopifyApp = User::where('name', '=', $request->domain)->first();
            if(!empty($shopifyApp)){
                $shopifyApp->update(['uninstall_date' => Carbon::now()]);
            }
        }
        else{
            logger('Uninstall: Verified false');
        }
        header("Status: 200");

    }

    public function view(Request $request){
        if(isset($request->shop) && !empty($request->shop)){
            $shopifyApp = User::where('name', '=', $request->shop)->first();
            if(!empty($shopifyApp)){
                $accountIDs = [];
                if(!empty($shopifyApp->acc_order_by)){
                    $accountIDs = json_decode(rawUrlDecode($shopifyApp->acc_order_by));
                }
                $contents = view('view', compact('shopifyApp', 'accountIDs'));
                $response = Response::make($contents);
                $response->header('Content-Type', 'application/javascript');
                return $response;
            }
        }
    }
    public function declined(Request $request){
        if ($request->session()->has('shop.id')) {
            $shopName = $request->session()->get('shop.name');
            return view('declined', compact('shopName'));
        }

        logger('Session failed! In declined');
        Session::flash('warning', 'Session failed!' );

        return redirect('/');
    }

    public function reinstall(Request $request){
        if ($request->session()->has('shop.id')) {
            $uninstall = $this->uninstallApp($request->session()->get('shop.name'), $request->session()->get('shop.token'));
            if($uninstall == 200){
                return redirect('/install?shop=' . $request->session()->get('shop.name'));
            }
            logger('Uninstall failed! In reinstall');
            Session::flash('warning', 'Uninstall failed!' );
            return redirect('/');
        }

        logger('Session failed! In reinstall');
        Session::flash('warning', 'Session failed!' );
        return redirect('/');
    }
    private function uninstallApp($shopName, $access_token){

        $revoke_url   = 'https://'. $shopName .'/admin/api_permissions/current.json';

        $headers = array(
            "Content-Type: application/json",
            "Accept: application/json",
            "Content-Length: 0",
            "X-Shopify-Access-Token: " . $access_token
        );

        $handler = curl_init($revoke_url);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);

        curl_exec($handler);
        if(!curl_errno($handler))
        {
            $info = curl_getinfo($handler);
        }
        $info = curl_getinfo($handler);

        curl_close($handler);

        return $info['http_code'];
    }
    // Verify Shopify oauth data
    private function verifyHmac($hmac) {
        $params = array();

        foreach($_GET as $param => $value) {
            if ($param != 'signature' && $param != 'hmac') {
                $params[$param] = "{$param}={$value}";
            }
        }

        asort($params);

        $params = implode('&', $params);
        $calculatedHmac = hash_hmac('sha256', $params, env('SHOPIFY_APP_SECRET'));

        return ($hmac == $calculatedHmac);
    }

}
