<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Session;

class MessageController extends Controller
{
    private $names = ['thank-you', 'confirm', 'reschedule', 'cancel', 'reminder'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $names = $this->names;
        return view('message.index', compact('names'));
    }


    public function show($name)
    {
        if(!in_array($name, $this->names)){
            return abort(404);
        }
        $message = Message::where(['name' => $name, 'user_id' => auth()->user()->id])->first();
        return view('message.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($name)
    {
        if(!in_array($name, $this->names)){
            return abort(404);
        }
        $message = Message::where(['name' => $name, 'user_id' => auth()->user()->id])->first();

        return view('message.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $name)
    {
        if(!in_array($name, $this->names)){
            return abort(404);
        }
        if($name == 'thank-you'){
            $this->validate($request, [
                'top_section' => 'required|max:100000',
                'print_button_name' => 'required|max:150',
                'button_name' => 'nullable|max:150',
                'button_url' => 'nullable|url|max:500',
                'button_new_tab' => 'nullable',
                'second_button_name' => 'nullable|max:150',
                'second_button_url' => 'nullable|url|max:500',
                'second_button_new_tab' => 'nullable'
            ]);
            $message = Message::firstOrNew(['name' => $name, 'user_id' => auth()->user()->id]);
            $message->top_section = $request->top_section;
            $message->options = json_encode([
                'print_button_name' => $request->print_button_name,
                'button_name' => $request->button_name,
                'button_url' => $request->button_url,
                'button_new_tab' => $request->button_new_tab,
                'second_button_name' => $request->second_button_name,
                'second_button_url' => $request->second_button_url,
                'second_button_new_tab' => $request->second_button_new_tab,
            ]);
        }
        else{
            $this->validate($request, [
                'header' => 'required|max:255',
                'top_section' => 'required|max:10000',
                'main_section' => 'required|max:500',
                'bottom_section' => 'required|max:10000',
            ]);
            $message = Message::firstOrNew(['name' => $name, 'user_id' => auth()->user()->id]);
            $message->header = $request->header;
            $message->top_section = $request->top_section;
            $message->main_section = $request->main_section;
            $message->bottom_section = $request->bottom_section;
        }
        $message->save();

        Session::flash('success', 'Message updated successfully!');

        return redirect('messages');
    }
}
